package com.zll.onlineExam;

import android.app.Application;

import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.iconics.Iconics;
import com.mikepenz.material_design_iconic_typeface_library.MaterialDesignIconic;

import org.xutils.x;

/**
 * @name MyApp
 * @date 2022/2/25 23:04
 */
public class MyApp extends Application {
    @Override
    public void onCreate() {

        super.onCreate();
        //初始化基础库
        initUI();

    }

    /**
     * 初始化XUI 框架
     */
    private void initUI() {

        //字体图标库
        Iconics.init(getApplicationContext());
        //这是自己定义的图标库
        Iconics.registerFont(new GoogleMaterial());
        Iconics.registerFont(new MaterialDesignIconic());

        x.Ext.init(this);
        x.Ext.setDebug(BuildConfig.DEBUG); // 是否输出debug日志, 开启debug会影响性能.
    }
}
