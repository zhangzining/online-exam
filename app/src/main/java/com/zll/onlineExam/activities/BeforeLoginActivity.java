package com.zll.onlineExam.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.LayoutInflaterCompat;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.mikepenz.iconics.context.IconicsLayoutInflater2;
import com.xuexiang.xui.widget.button.roundbutton.RoundButton;
import com.zll.onlineExam.R;

public class BeforeLoginActivity extends AppCompatActivity implements View.OnClickListener {

    private static String TAG ="===BeforeLoginActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        LayoutInflaterCompat.setFactory2(getLayoutInflater(), new IconicsLayoutInflater2(getDelegate()));
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_before_login);
        RoundButton login = findViewById(R.id.a_bo_b_login);
        RoundButton register = findViewById(R.id.a_bo_b_register);

        login.setOnClickListener(this);
        register.setOnClickListener(this);

    }


    @Override
    public void onClick(View view) {
        Intent it = null;
        if (view.getId() == R.id.a_bo_b_login){
            it = new Intent(this,LoginActivity.class);
        }else if (view.getId() == R.id.a_bo_b_register){
            it = new Intent(this,RegisterActivity.class);
        }

        startActivity(it);
    }

}