package com.zll.onlineExam.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.LayoutInflaterCompat;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;

import com.alibaba.fastjson.JSONObject;
import com.mikepenz.iconics.context.IconicsLayoutInflater2;
import com.xuexiang.xui.widget.button.roundbutton.RoundButton;
import com.xuexiang.xui.widget.edittext.materialedittext.MaterialEditText;
import com.zll.onlineExam.R;
import com.zll.onlineExam.ajax.Ajax;
import com.zll.onlineExam.ajax.HTTPConfig;
import com.zll.onlineExam.ajax.ReqCallback;
import com.zll.onlineExam.db.DBHelper;
import com.zll.onlineExam.entity.out.UserDtoOut;
import com.zll.onlineExam.utils.GlobalDataHolder;
import com.zll.onlineExam.utils.XToastUtils;


import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private MaterialEditText mUsername;
    private MaterialEditText mPassword;
    private RoundButton mButton;
    private Handler mHandler;
    private final int LOGIN_SUCCESS = 1;
    private final int LOGIN_FAIL = 0;
    private final int LOGIN_ERROR = -1;

    private static String TAG = "===LoginActivity";
    private SQLiteDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        LayoutInflaterCompat.setFactory2(getLayoutInflater(), new IconicsLayoutInflater2(getDelegate()));
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initView();
        initListener();
        initHandler();

        db = DBHelper.getDB(LoginActivity.this);

    }

    private void initHandler() {
        mHandler = new Handler() {
            @Override
            public void handleMessage(@NonNull Message msg) {
                super.handleMessage(msg);

                Intent it;

                switch (msg.what) {
                    case LOGIN_SUCCESS:
                        XToastUtils.success("登录成功");
                        it = new Intent(LoginActivity.this, MainActivity.class);
                        startActivity(it);
                        finish();
                        break;
                    case LOGIN_FAIL:
                        XToastUtils.error("账号或密码错误");
                        break;
                    case LOGIN_ERROR:
                        XToastUtils.error("网络异常");
                        break;
                    default:
                        break;

                }

            }
        };
    }

    private void initView() {
        mUsername = findViewById(R.id.a_login_username);
        mPassword = findViewById(R.id.a_login_password);
        mButton = findViewById(R.id.a_login_b_login);
    }

    private void initListener() {
        mButton.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.a_login_b_login) {
            doLogin();
            XToastUtils.info("正在加载");
        }

    }

    private void doLogin() {
        String username = mUsername.getEditValue();
        String password = mPassword.getEditValue();
        Map<String, Object> data = new HashMap<>();
        data.put("username", username);
        data.put("password", password);
        Ajax.post(HTTPConfig.LOGIN, data, new ReqCallback() {
            @Override
            public void onSuccess(JSONObject resp) {
                GlobalDataHolder.getInstance().setCurUser(resp.getObject("data", UserDtoOut.class));
                DBHelper.initUser(db, resp.getObject("data", Map.class));
                Ajax.setToken(DBHelper.checkDBAndGetToken(db));
                mHandler.sendEmptyMessage(LOGIN_SUCCESS);
            }

            @Override
            public void onFail(String msg) {
                mHandler.sendEmptyMessage(LOGIN_FAIL);
            }

            @Override
            public void onError(String msg) {
                mHandler.sendEmptyMessage(LOGIN_ERROR);
            }
        });
    }
}