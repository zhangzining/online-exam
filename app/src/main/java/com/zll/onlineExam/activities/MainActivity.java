package com.zll.onlineExam.activities;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.mikepenz.iconics.context.IconicsLayoutInflater2;
import com.zll.onlineExam.R;
import com.zll.onlineExam.adapter.MainViewPagerAdapter;
import com.zll.onlineExam.fragments.AccountFragment;
import com.zll.onlineExam.fragments.ExamFragment;
import com.zll.onlineExam.fragments.LearningFragment;
import com.zll.onlineExam.fragments.StoreFragment;
import com.zll.onlineExam.utils.XToastUtils;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.LayoutInflaterCompat;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener,BottomNavigationView.OnNavigationItemSelectedListener {

    private ViewPager viewPager;
    private BottomNavigationView bottomNavigationView;
    private Toolbar toolbar;
    private List<Fragment> pages;
    private List<String> titles;

    private static String TAG = "===MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        LayoutInflaterCompat.setFactory2(getLayoutInflater(), new IconicsLayoutInflater2(getDelegate()));
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        initListeners();
        getPermission();

    }

    private void initListeners() {
        pages = new ArrayList<>();
        pages.add(new LearningFragment());
        pages.add(new ExamFragment());
        pages.add(new StoreFragment());
        pages.add(new AccountFragment());

        titles = Arrays.asList("学习","答题","商城","我的");

        viewPager.setAdapter(new MainViewPagerAdapter(getSupportFragmentManager(),pages));
        viewPager.setCurrentItem(R.layout.fragment_learning,false);
        viewPager.addOnPageChangeListener(this);

        bottomNavigationView.setOnNavigationItemSelectedListener(this);

        viewPager.setCurrentItem(0,false);

    }

    private void initView() {
        toolbar = findViewById(R.id.toolbar);
        viewPager = findViewById(R.id.view_pager);
        bottomNavigationView = findViewById(R.id.bottom_navigation);
    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        switch (position){
            case 0:
                bottomNavigationView.setSelectedItemId(R.id.nav_learning);
                toolbar.setTitle(titles.get(0));
                break;
            case 1:
                bottomNavigationView.setSelectedItemId(R.id.nav_exam);
                toolbar.setTitle(titles.get(1));
                break;
            case 2:
                bottomNavigationView.setSelectedItemId(R.id.nav_store);
                toolbar.setTitle(titles.get(2));
                break;
            case 3:
                bottomNavigationView.setSelectedItemId(R.id.nav_account);
                toolbar.setTitle(titles.get(3));
                break;
        }

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int itemId = item.getItemId();
        switch (itemId){
            case R.id.nav_learning:
                viewPager.setCurrentItem(0,false);
                toolbar.setTitle(titles.get(0));
                return true;
            case R.id.nav_exam:
                viewPager.setCurrentItem(1,false);
                toolbar.setTitle(titles.get(1));
                return true;
            case R.id.nav_store:
                viewPager.setCurrentItem(2,false);
                toolbar.setTitle(titles.get(2));
                return true;
            case R.id.nav_account:
                viewPager.setCurrentItem(3,false);
                toolbar.setTitle(titles.get(3));
                return true;
        }

        return false;
    }

    private void getPermission() {
        //如果没有权限，Android6.0之后，必须动态申请权限（记住这个套路）
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            //如果用户已经拒绝过一次权限申请，该方法返回true
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                //提示用户这一权限的重要性
                XToastUtils.info("读取权限用于选择并上传头像");
            }
            //请求权限
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
        } else { //权限已被授予，打开相册
//            XToastUtils.info("读取权限已获得");
            Log.d(TAG, "getPermission: 读取权限已获得");
        }
    }

    @Override
    //弹出一个权限申请的对话框，并且用户做出选择后，该方法自动回调
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1:
                if (grantResults.length <= 0 || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    XToastUtils.error("获取权限失败");
                }
                break;
            default:
                break;
        }
    }


}