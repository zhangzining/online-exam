package com.zll.onlineExam.activities;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.alibaba.fastjson.JSONObject;
import com.zll.onlineExam.R;
import com.zll.onlineExam.ajax.Ajax;
import com.zll.onlineExam.ajax.HTTPConfig;
import com.zll.onlineExam.ajax.ReqCallback;
import com.zll.onlineExam.db.DBHelper;
import com.zll.onlineExam.entity.out.UserDtoOut;
import com.zll.onlineExam.utils.GlobalDataHolder;
import com.zll.onlineExam.utils.XToastUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * 开屏页
 *
 * @name OpeningActivity
 * @date 2022/2/25 22:53
 */
public class OpeningActivity extends AppCompatActivity {
    private Handler mHandler;
    private static String TAG = "===OpeningActivity";

    private final int FLUSH_SUCCESS = 1;
    private final int FLUSH_FAIL = 0;
    private final int FLUSH_ERROR = -1;

    private SQLiteDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_opening);

        db = DBHelper.getDB(OpeningActivity.this);

        Ajax.setToken(DBHelper.checkDBAndGetToken(db));

        initLogin();
        getUserInfo();


    }

    private void getUserInfo() {
        Ajax.post(HTTPConfig.FlUSH, new HashMap<>(), new ReqCallback() {
            @Override
            public void onSuccess(JSONObject resp) {
                GlobalDataHolder.getInstance().setCurUser(resp.getObject("data", UserDtoOut.class));
                DBHelper.initUser(db, resp.getObject("data", Map.class));
                Ajax.setToken(DBHelper.checkDBAndGetToken(db));
                mHandler.sendEmptyMessage(FLUSH_SUCCESS);
            }

            @Override
            public void onFail(String msg) {
                mHandler.sendEmptyMessage(FLUSH_FAIL);
            }

            @Override
            public void onError(String msg) {
                mHandler.sendEmptyMessage(FLUSH_ERROR);
            }
        });
    }

    private void initLogin() {
        mHandler = new Handler() {
            @Override
            public void handleMessage(@NonNull Message msg) {
                super.handleMessage(msg);

                Intent it;

                switch (msg.what) {
                    case FLUSH_SUCCESS:
                        it = new Intent(OpeningActivity.this, MainActivity.class);
                        break;
                    case FLUSH_FAIL:
                        it = new Intent(OpeningActivity.this, BeforeLoginActivity.class);
                        break;
                    case FLUSH_ERROR:
                        it = new Intent(OpeningActivity.this, BeforeLoginActivity.class);
                        XToastUtils.error("网络异常");
                        break;
                    default:
                        it = new Intent(OpeningActivity.this, BeforeLoginActivity.class);
                        break;

                }
                startActivity(it);
                OpeningActivity.this.finish();

            }
        };

    }

}
