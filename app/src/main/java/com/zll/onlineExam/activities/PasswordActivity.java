package com.zll.onlineExam.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.LayoutInflaterCompat;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.ImageView;

import com.alibaba.fastjson.JSONObject;
import com.mikepenz.iconics.context.IconicsLayoutInflater2;
import com.xuexiang.xui.widget.edittext.materialedittext.MaterialEditText;
import com.xuexiang.xui.widget.edittext.materialedittext.validation.RegexpValidator;
import com.zll.onlineExam.R;
import com.zll.onlineExam.ajax.Ajax;
import com.zll.onlineExam.ajax.HTTPConfig;
import com.zll.onlineExam.ajax.ReqCallback;
import com.zll.onlineExam.entity.out.UserDtoOut;
import com.zll.onlineExam.utils.GlobalDataHolder;
import com.zll.onlineExam.utils.XToastUtils;


import java.util.HashMap;
import java.util.Map;

public class PasswordActivity extends AppCompatActivity implements View.OnClickListener {

    private MaterialEditText mPassword;
    private MaterialEditText mRePassword;
    private MaterialEditText mOldPassword;
    private ImageView mBack;
    private ImageView mCommit;

    private final int COMMIT_SUCCESS = 1;
    private final int COMMIT_FAIL = 0;
    private final int COMMIT_ERROR = -1;

    private static Handler mHandler;
    private static final String TAG = "===PasswordActivity";
    private final static String pattern = "^(?![0-9]+$)(?![a-zA-Z]+$)(?!([^(0-9a-zA-Z)])+$).{6,12}$";

    private Map<String,Object> userInfo ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        LayoutInflaterCompat.setFactory2(getLayoutInflater(), new IconicsLayoutInflater2(getDelegate()));
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password);

        initView();
        initListener();
        initHandler();
        initData();



    }

    private void initView() {
        mBack = findViewById(R.id.iv_back);
        mCommit = findViewById(R.id.iv_commit);
        mPassword = findViewById(R.id.a_info_password);
        mRePassword = findViewById(R.id.a_info_repassword);
        mOldPassword = findViewById(R.id.a_info_oldpassword);

    }

    private void initListener() {
        mBack.setOnClickListener(this);
        mCommit.setOnClickListener(this);

        mPassword.addValidator(new RegexpValidator("格式不对",pattern));
        mRePassword.addValidator(new RegexpValidator("格式不对",pattern));
    }

    private void initHandler() {
        mHandler = new Handler(){
            public void handleMessage(@NonNull Message msg) {
                super.handleMessage(msg);
                mCommit.setEnabled(true);
                switch (msg.what) {
                    case COMMIT_SUCCESS:
                        XToastUtils.success("提交成功");
                        finish();
                        break;
                    case COMMIT_FAIL:
                        XToastUtils.error("密码错误");
                        break;
                    case COMMIT_ERROR:
                        XToastUtils.error("网络异常");
                        break;
                    default:
                        break;
                }

            }
        };
    }

    private void initData() {
        userInfo = new HashMap<>();
        userInfo.put("uid", GlobalDataHolder.getInstance().getCurUser().getUid());
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.iv_back){
            finish();
        }
        if (id == R.id.iv_commit){
            commitInfos();
        }
    }

    private void commitInfos() {
        if (mPassword.validate() && mRePassword.validate() && mOldPassword.isNotEmpty()){
            String passwd = mPassword.getEditValue();
            String rePasswd = mRePassword.getEditValue();
            String oldPasswd = mOldPassword.getEditValue();
            if (!passwd.equals(rePasswd)){
                XToastUtils.info("两次输入的密码不同");
                return;
            }
            userInfo.put("oldPassword",oldPasswd);
            userInfo.put("newPassword",passwd);

            XToastUtils.info("正在提交，请稍候");

            Ajax.post(HTTPConfig.CHANGE_INFO, userInfo, new ReqCallback() {
                @Override
                public void onSuccess(JSONObject resp) {
                    GlobalDataHolder.getInstance().setCurUser(resp.getObject("data", UserDtoOut.class));
                    mHandler.sendEmptyMessage(COMMIT_SUCCESS);
                }

                @Override
                public void onFail(String msg) {
                    mHandler.sendEmptyMessage(COMMIT_FAIL);
                }

                @Override
                public void onError(String msg) {
                    mHandler.sendEmptyMessage(COMMIT_ERROR);
                }
            });
        }else{
            XToastUtils.error("请先填写信息");
        }
    }

}