package com.zll.onlineExam.adapter;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.zll.onlineExam.R;
import com.zll.onlineExam.adapter.clickListener.ItemViewClickListener;
import com.zll.onlineExam.adapter.holder.ExamItemViewHolder;
import com.zll.onlineExam.entity.out.PaperDtoOut;
import com.zll.onlineExam.utils.StringUtil;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @name LearningRecycleViewAdapter
 * @date 2022/3/8 11:30
 */
public class ExamItemViewAdapter extends RecyclerView.Adapter<ExamItemViewHolder> {

    private List<PaperDtoOut> dataList;
    private ItemViewClickListener itemClickListener;
    private Context context;


    public ExamItemViewAdapter(List<PaperDtoOut> dataList, ItemViewClickListener listener, Context context) {
        this.dataList = dataList;
        this.itemClickListener = listener;
        this.context = context;
    }

    public List<PaperDtoOut> getDataList() {
        return dataList;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void setData(List<PaperDtoOut> newList){
        this.dataList = newList.stream().filter(item->{
            if("today".equals(item.getType())) {
                LocalDate expire = LocalDate.parse(item.getExpireTime(), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
                return LocalDate.now().isEqual(expire);
            }
            return true;
        }).collect(Collectors.toList());

        this.notifyDataSetChanged();
    }

    public void expandData(List<PaperDtoOut> newList){
        this.dataList.addAll(newList);
        this.notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ExamItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ExamItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_exam_card_view_list_item,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull ExamItemViewHolder holder, int position) {
        PaperDtoOut dto = dataList.get(position);
        holder.mDuration.setText(
                "不限时".equals(dto.getDuration())?dto.getDuration():dto.getDuration()+" 分钟"
        );
        if(StringUtil.isNotBlank(dto.getUserTotalScore())){
            holder.mDuration.setText(holder.mDuration.getText()+" 用时 "+dto.getUserCostTime()+" 分钟");
            String res = "得分 "+dto.getUserTotalScore()+" 答对 "+dto.getUserCorrectCount()+" 题";
            holder.mResult.setText(res);
        }
        if("today".equals(dto.getType())){
            holder.mTag.setText("今日答题");
            holder.mTag.setTextColor(context.getResources().getColor(R.color.red));
        }else {
            holder.mTag.setText(dto.getTag());
        }
        holder.mTitle.setText(dto.getTitle());
        holder.mSummary.setText(dto.getSummary());
        holder.mExpire.setText(dto.getExpireTime()+" 过期");
        holder.mCount.setText(dto.getQuestionCount() +" 题");
        holder.view.setOnClickListener(i->itemClickListener.onItemClick(dataList.get(position).getPid().toString()));
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }
}
