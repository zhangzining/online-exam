package com.zll.onlineExam.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.zll.onlineExam.R;
import com.zll.onlineExam.adapter.holder.LearningItemViewHolder;
import com.zll.onlineExam.adapter.clickListener.ItemViewClickListener;
import com.zll.onlineExam.ajax.HTTPConfig;
import com.zll.onlineExam.entity.out.ArticleDtoOut;
import com.zll.onlineExam.utils.StringUtil;


import org.xutils.image.ImageOptions;
import org.xutils.x;

import java.text.SimpleDateFormat;
import java.util.List;

/**
 * @name LearningRecycleViewAdapter
 * @date 2022/3/8 11:30
 */
public class LearningItemViewAdapter extends RecyclerView.Adapter<LearningItemViewHolder> {

    private List<ArticleDtoOut> dataList;
    private ItemViewClickListener itemClickListener;
    private ImageOptions imageOptions;
    private boolean historyMode;


    public LearningItemViewAdapter(List<ArticleDtoOut> dataList, ItemViewClickListener listener, boolean historyMode) {
        this.dataList = dataList;
        this.itemClickListener = listener;
        this.historyMode = historyMode;
        this.imageOptions = new ImageOptions.Builder()
                // 如果ImageView的大小不是定义为wrap_content, 不要crop.
                .setCrop(true) // 很多时候设置了合适的scaleType也不需要它.
                // 加载中或错误图片的ScaleType
                //.setPlaceholderScaleType(ImageView.ScaleType.MATRIX)
                .setImageScaleType(ImageView.ScaleType.CENTER_CROP)
                .setPlaceholderScaleType(ImageView.ScaleType.CENTER_CROP)
                .setLoadingDrawableId(R.drawable.ic_default_head)
                .setFailureDrawableId(R.drawable.ic_default_head)
                .setIgnoreGif(false)
                .build();
    }

    public List<ArticleDtoOut> getDataList() {
        return dataList;
    }

    public void setData(List<ArticleDtoOut> newList) {
        this.dataList = newList;
        this.notifyDataSetChanged();
    }

    public void expandData(List<ArticleDtoOut> newList) {
        this.dataList.addAll(newList);
        this.notifyDataSetChanged();
    }

    @NonNull
    @Override
    public LearningItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new LearningItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_article_card_view_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull LearningItemViewHolder holder, int position) {
        ArticleDtoOut dto = dataList.get(position);
        holder.mUsername.setText(dto.getPublisherUser().getNickname());
        x.image().bind(holder.mAvatar, dto.getPublisherUser().getAvatar(HTTPConfig.API + HTTPConfig.IMG_PREFIX), imageOptions);
        if(StringUtil.isNotBlank(dto.getCoverImage())){
            x.image().bind(holder.mImg, HTTPConfig.API + HTTPConfig.IMG_PREFIX+dto.getCoverImage(), imageOptions);
        }

        holder.mTitle.setText(dto.getTitle());

        holder.mSummary.setText(dto.getTags());
        holder.mComments.setText(String.valueOf(dto.getCommentCount() == null ? 0 : dto.getCommentCount()));
        holder.mApprove.setText(String.valueOf(dto.getApproveCount() == null ? 0 : dto.getApproveCount()));
        holder.view.setOnClickListener(i -> itemClickListener.onItemClick(dataList.get(position).getAid().toString()));

        if (historyMode) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            holder.mCreateTime.setText(sdf.format(dto.getCreateTime()));
            holder.mCreateInfo.setVisibility(View.VISIBLE);
            holder.mBottomInfo.setVisibility(View.GONE);
            if (dto.getGainBonusScore() != 0) {
                holder.mScore.setText(String.valueOf("获得 " + dto.getGainBonusScore()) + " 分");
            }
        }

    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }
}
