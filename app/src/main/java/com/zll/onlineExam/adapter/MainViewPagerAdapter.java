package com.zll.onlineExam.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.PagerAdapter;

import java.util.List;

/**
 * @name MainViewPagerAdapter
 * @date 2022/3/5 12:48
 */
public class MainViewPagerAdapter extends FragmentStatePagerAdapter {

    List<Fragment> pages ;

    public MainViewPagerAdapter(@NonNull FragmentManager fm, List<Fragment> pages) {
        super(fm, FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        this.pages = pages;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        return pages.get(position);
    }

    @Override
    public int getCount() {
        return pages.size();
    }
}
