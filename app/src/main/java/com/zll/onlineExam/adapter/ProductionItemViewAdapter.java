package com.zll.onlineExam.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.zll.onlineExam.R;
import com.zll.onlineExam.adapter.clickListener.ItemViewClickListener;
import com.zll.onlineExam.adapter.holder.ProductionItemViewHolder;
import com.zll.onlineExam.ajax.HTTPConfig;
import com.zll.onlineExam.entity.out.ProductionDtoOut;
import com.zll.onlineExam.utils.StringUtil;

import org.xutils.image.ImageOptions;
import org.xutils.x;

import java.text.SimpleDateFormat;
import java.util.List;

/**
 * @name LearningRecycleViewAdapter
 * @date 2022/3/8 11:30
 */
public class ProductionItemViewAdapter extends RecyclerView.Adapter<ProductionItemViewHolder> {

    private List<ProductionDtoOut> dataList;
    private ItemViewClickListener itemClickListener;
    private boolean historyMode;


    public ProductionItemViewAdapter(List<ProductionDtoOut> dataList, ItemViewClickListener listener,boolean historyMode) {
        this.dataList = dataList;
        this.itemClickListener = listener;
        this.historyMode = historyMode;
    }

    public List<ProductionDtoOut> getDataList() {
        return dataList;
    }

    public void setData(List<ProductionDtoOut> newList){
        this.dataList = newList;
        this.notifyDataSetChanged();
    }

    public void expandData(List<ProductionDtoOut> newList){
        this.dataList.addAll(newList);
        this.notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ProductionItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ProductionItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_production_card_view_list_item,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull ProductionItemViewHolder holder, int position) {
        ProductionDtoOut dto = dataList.get(position);
        holder.mTitle.setText(dto.getTitle());
        holder.mScore.setText(dto.getBonusScore()+"积分");
        String priceAndType = dto.getType()+" | 价值 "+dto.getPrice()+" 元";
        holder.mPrice.setText(priceAndType);

        holder.view.setOnClickListener(i->itemClickListener.onItemClick(dataList.get(position).getPid().toString()));

        if (dto.getPictureList() != null && dto.getPictureList().size()>0) {
            String imgName = dto.getPictureList().get(0);
            ImageOptions imageOptions = new ImageOptions.Builder()
                    // 如果ImageView的大小不是定义为wrap_content, 不要crop.
                    .setCrop(true) // 很多时候设置了合适的scaleType也不需要它.
                    // 加载中或错误图片的ScaleType
                    //.setPlaceholderScaleType(ImageView.ScaleType.MATRIX)
                    .setImageScaleType(ImageView.ScaleType.CENTER_CROP)
                    .setPlaceholderScaleType(ImageView.ScaleType.CENTER_CROP)
                    .setLoadingDrawableId(R.drawable.ic_default_img)
                    .setFailureDrawableId(R.drawable.ic_default_img)
                    .setIgnoreGif(false)
                    .build();
            if (!imgName.startsWith("http")) {
                imgName = HTTPConfig.API+ HTTPConfig.IMG_PREFIX + imgName;
            }
            x.image().bind(holder.mImage,imgName,imageOptions);
        }

        if (historyMode){
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            holder.mCreateTime.setText(sdf.format(dto.getCreateTime()));
            holder.mCreateInfo.setVisibility(View.VISIBLE);
            holder.mScore.setText(dto.getExchangeScore()+"积分");
        }

    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }
}
