package com.zll.onlineExam.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.zll.onlineExam.R;
import com.zll.onlineExam.adapter.holder.QuestionItemViewHolder;
import com.zll.onlineExam.entity.ServiceConstant;
import com.zll.onlineExam.entity.out.QuestionDtoOut;
import com.zll.onlineExam.utils.StringUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * @name LearningRecycleViewAdapter
 * @date 2022/3/8 11:30
 */
public class QuestionItemViewAdapter extends RecyclerView.Adapter<QuestionItemViewHolder> {

    private List<QuestionDtoOut> dataList;
    private List<QuestionItemViewHolder> holders;


    public QuestionItemViewAdapter(List<QuestionDtoOut> dataList) {
        holders = new ArrayList<>();
        this.dataList = dataList;
    }

    public List<QuestionDtoOut> getDataList() {
        return dataList;
    }

    public void setData(List<QuestionDtoOut> newList) {
        this.dataList = newList;
        this.notifyDataSetChanged();
    }

    public void expandData(List<QuestionDtoOut> newList) {
        this.dataList.addAll(newList);
        this.notifyDataSetChanged();
    }

    public List<QuestionItemViewHolder> getHolders() {
        return holders;
    }

    @NonNull
    @Override
    public QuestionItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        return new QuestionItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_question_card_view_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull QuestionItemViewHolder holder, int position) {
        QuestionDtoOut dto = dataList.get(position);

        holder.mOrder.setText("第 " + (position + 1) + " 题");
        holder.mTitle.setText(dto.getTitle());
        holder.mScore.setText(dto.getScore() + " 分");

        if (ServiceConstant.MULTI.equals(dto.getType())) {
            // 多选
            holder.mC1.setText(dto.getOption1());
            holder.mC2.setText(dto.getOption2());
            holder.mC3.setText(dto.getOption3());
            holder.mC4.setText(dto.getOption4());
            holder.mMulti.setVisibility(View.VISIBLE);
        }
        if (ServiceConstant.SINGLE.equals(dto.getType())) {
            // 单选
            holder.mRD1.setText(dto.getOption1());
            holder.mRD2.setText(dto.getOption2());
            holder.mRD3.setText(dto.getOption3());
            holder.mRD4.setText(dto.getOption4());
            holder.mSingle.setVisibility(View.VISIBLE);
        }
        if (ServiceConstant.CHOOSE.equals(dto.getType())) {
            // 判断
            holder.mChoose.setVisibility(View.VISIBLE);
        }
        if (ServiceConstant.FILL.equals(dto.getType())) {
            // 填空
            holder.mFill.setVisibility(View.VISIBLE);
        }

        if (StringUtil.isNotBlank(dto.getUserAnswers())) {
            // 展示模式
            if (dto.getIsCorrect() == ServiceConstant.IS_CORRECT) {
                holder.mRight.setVisibility(View.VISIBLE);
            } else {
                holder.mWrong.setVisibility(View.VISIBLE);
            }
            if (ServiceConstant.MULTI.equals(dto.getType())) {
                // 多选
                String answers = dto.getUserAnswers();
                for (String s : answers.split(",")) {
                    holder.mC1.setEnabled(false);
                    holder.mC2.setEnabled(false);
                    holder.mC3.setEnabled(false);
                    holder.mC4.setEnabled(false);
                    if (s.equals("A")) {
                        holder.mC1.setChecked(true);
                    }
                    if (s.equals("B")) {
                        holder.mC2.setChecked(true);
                    }
                    if (s.equals("C")) {
                        holder.mC3.setChecked(true);
                    }
                    if (s.equals("D")) {
                        holder.mC4.setChecked(true);
                    }
                }
            }
            if (ServiceConstant.SINGLE.equals(dto.getType())) {
                // 单选
                holder.mRD1.setEnabled(false);
                holder.mRD2.setEnabled(false);
                holder.mRD3.setEnabled(false);
                holder.mRD4.setEnabled(false);
                String s = dto.getUserAnswers();
                if (s.equals("A")) {
                    holder.mRD1.setChecked(true);
                }
                if (s.equals("B")) {
                    holder.mRD2.setChecked(true);
                }
                if (s.equals("C")) {
                    holder.mRD3.setChecked(true);
                }
                if (s.equals("D")) {
                    holder.mRD4.setChecked(true);
                }
            }
            if (ServiceConstant.CHOOSE.equals(dto.getType())) {
                // 判断
                holder.mRDt.setEnabled(false);
                holder.mRDf.setEnabled(false);
                String s = dto.getUserAnswers();
                if (s.equals("A")) {
                    holder.mRDt.setChecked(true);
                }
                if (s.equals("B")) {
                    holder.mRDf.setChecked(true);
                }
            }
            if (ServiceConstant.FILL.equals(dto.getType())) {
                // 填空
                holder.mTextInput.setText(dto.getUserAnswers());
                holder.mTextInput.setEnabled(false);
            }
        }

        holders.add(holder);
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }
}
