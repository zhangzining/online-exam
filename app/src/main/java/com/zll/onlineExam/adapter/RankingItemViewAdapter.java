package com.zll.onlineExam.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.zll.onlineExam.R;
import com.zll.onlineExam.adapter.clickListener.ItemViewClickListener;
import com.zll.onlineExam.adapter.holder.RankingItemViewHolder;
import com.zll.onlineExam.ajax.HTTPConfig;
import com.zll.onlineExam.entity.out.UserDtoOut;
import com.zll.onlineExam.utils.GlobalDataHolder;
import com.zll.onlineExam.utils.StringUtil;

import org.xutils.image.ImageOptions;
import org.xutils.x;

import java.text.SimpleDateFormat;
import java.util.List;

/**
 * @name LearningRecycleViewAdapter
 * @date 2022/3/8 11:30
 */
public class RankingItemViewAdapter extends RecyclerView.Adapter<RankingItemViewHolder> {

    private List<UserDtoOut> dataList;
    private Context context;


    public RankingItemViewAdapter(List<UserDtoOut> dataList,Context context) {
        this.dataList = dataList;
        this.context = context;
    }

    public List<UserDtoOut> getDataList() {
        return dataList;
    }

    public void setData(List<UserDtoOut> newList){
        this.dataList = newList;
        this.notifyDataSetChanged();
    }

    public void expandData(List<UserDtoOut> newList){
        this.dataList.addAll(newList);
        this.notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RankingItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RankingItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_ranking_card_view_list_item,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull RankingItemViewHolder holder, int position) {
        UserDtoOut dto = dataList.get(position);
        if (dto.getUid().equals(GlobalDataHolder.getInstance().getCurUser().getUid())){
            holder.mNumber.setText((position+1)+"*");
            holder.mNumber.setTextColor(context.getResources().getColor(R.color.red));
        }else{
            holder.mNumber.setText(String.valueOf(position+1));
        }
        if (dto.getNickname().length() > 5){
            String nickname = dto.getNickname();
            String res = nickname.substring(0,2)+"***"+nickname.substring(nickname.length()-2);
            holder.mNickname.setText(res);
        }else{
            holder.mNickname.setText(dto.getNickname());
        }
        holder.mReadCount.setText(StringUtil.from(dto.getReadCount()));
        holder.mExamCount.setText(StringUtil.from(dto.getExamCount()));

        String imgName = dto.getAvatar();
        ImageOptions imageOptions = new ImageOptions.Builder()
                // 如果ImageView的大小不是定义为wrap_content, 不要crop.
                .setCrop(true) // 很多时候设置了合适的scaleType也不需要它.
                // 加载中或错误图片的ScaleType
                //.setPlaceholderScaleType(ImageView.ScaleType.MATRIX)
                .setImageScaleType(ImageView.ScaleType.CENTER_CROP)
                .setPlaceholderScaleType(ImageView.ScaleType.CENTER_CROP)
                .setLoadingDrawableId(R.drawable.ic_default_head)
                .setFailureDrawableId(R.drawable.ic_default_head)
                .setIgnoreGif(false)
                .build();
        if (!imgName.startsWith("http")) {
            imgName = HTTPConfig.API+ HTTPConfig.IMG_PREFIX + imgName;
        }
        x.image().bind(holder.mAvatar,imgName,imageOptions);
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }
}
