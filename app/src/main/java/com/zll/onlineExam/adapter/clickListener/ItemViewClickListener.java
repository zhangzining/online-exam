package com.zll.onlineExam.adapter.clickListener;

/**
 * @name ItemViewClickListener
 * @date 2022/3/8 15:56
 */
public interface ItemViewClickListener {
    void onItemClick(String aid);
}
