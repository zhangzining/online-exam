package com.zll.onlineExam.adapter.holder;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.xuexiang.xui.widget.imageview.RadiusImageView;
import com.zll.onlineExam.R;

/**
 * @name LearningItemViewHolder
 * @date 2022/3/8 11:35
 */
public class ExamItemViewHolder extends RecyclerView.ViewHolder {

    public TextView mDuration;
    public TextView mTag;
    public TextView mTitle;
    public TextView mSummary;
    public TextView mCount;
    public TextView mExpire;
    public TextView mResult;
    public View view;


    public ExamItemViewHolder(@NonNull View itemView) {
        super(itemView);

        mDuration = itemView.findViewById(R.id.tv_exam_duration);
        mTag = itemView.findViewById(R.id.tv_tag);
        mTitle = itemView.findViewById(R.id.tv_title);
        mSummary = itemView.findViewById(R.id.tv_summary);
        mCount = itemView.findViewById(R.id.tv_count);
        mExpire = itemView.findViewById(R.id.tv_expire);
        mResult = itemView.findViewById(R.id.tv_result);
        view = itemView;
    }
}
