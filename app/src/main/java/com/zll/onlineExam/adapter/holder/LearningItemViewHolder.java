package com.zll.onlineExam.adapter.holder;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.xuexiang.xui.widget.imageview.RadiusImageView;
import com.zll.onlineExam.R;

/**
 * @name LearningItemViewHolder
 * @date 2022/3/8 11:35
 */
public class LearningItemViewHolder extends RecyclerView.ViewHolder {

    public RadiusImageView mAvatar;
    public TextView mUsername;
    public TextView mTag;
    public TextView mTitle;
    public TextView mSummary;
    public TextView mApprove;
    public TextView mComments;
    public TextView mCreateTime;
    public TextView mScore;
    public RadiusImageView mImg;
    public LinearLayout mBottomInfo;
    public LinearLayout mCreateInfo;
    public View view;


    public LearningItemViewHolder(@NonNull View itemView) {
        super(itemView);

        mAvatar = itemView.findViewById(R.id.iv_avatar);
        mImg = itemView.findViewById(R.id.iv_image);
        mUsername = itemView.findViewById(R.id.tv_user_name);
        mTag = itemView.findViewById(R.id.tv_tag);
        mTitle = itemView.findViewById(R.id.tv_title);
        mSummary = itemView.findViewById(R.id.tv_summary);
        mApprove = itemView.findViewById(R.id.tv_praise);
        mComments = itemView.findViewById(R.id.tv_comment);
        mCreateTime = itemView.findViewById(R.id.tv_create);
        mScore = itemView.findViewById(R.id.tv_score_gain);
        mBottomInfo = itemView.findViewById(R.id.b_icons);
        mCreateInfo = itemView.findViewById(R.id.b_info);

        view = itemView;
    }
}
