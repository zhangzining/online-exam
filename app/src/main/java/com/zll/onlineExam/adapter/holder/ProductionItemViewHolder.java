package com.zll.onlineExam.adapter.holder;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.xuexiang.xui.widget.imageview.RadiusImageView;
import com.zll.onlineExam.R;

/**
 * @name LearningItemViewHolder
 * @date 2022/3/8 11:35
 */
public class ProductionItemViewHolder extends RecyclerView.ViewHolder {

    public TextView mTitle;
    public TextView mPrice;
    public TextView mScore;
    public RadiusImageView mImage;
    public View view;
    public TextView mCreateTime;
    public LinearLayout mCreateInfo;


    public ProductionItemViewHolder(@NonNull View itemView) {
        super(itemView);

        mTitle = itemView.findViewById(R.id.tv_title);
        mPrice = itemView.findViewById(R.id.tv_type_price);
        mScore = itemView.findViewById(R.id.tv_score);
        mImage = itemView.findViewById(R.id.iv_image);
        view = itemView;
        mCreateTime = itemView.findViewById(R.id.tv_create);
        mCreateInfo = itemView.findViewById(R.id.b_info);
    }
}
