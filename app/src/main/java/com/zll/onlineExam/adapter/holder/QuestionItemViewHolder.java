package com.zll.onlineExam.adapter.holder;

import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.xuexiang.xui.widget.edittext.materialedittext.MaterialEditText;
import com.zll.onlineExam.R;

/**
 * @name LearningItemViewHolder
 * @date 2022/3/8 11:35
 */
public class QuestionItemViewHolder extends RecyclerView.ViewHolder {

    public TextView mOrder;
    public TextView mScore;
    public TextView mRight;
    public TextView mWrong;
    public TextView mTitle;

    public LinearLayout mMulti;
    public LinearLayout mSingle;
    public LinearLayout mChoose;
    public LinearLayout mFill;

    public CheckBox mC1;
    public CheckBox mC2;
    public CheckBox mC3;
    public CheckBox mC4;

    public RadioButton mRD1;
    public RadioButton mRD2;
    public RadioButton mRD3;
    public RadioButton mRD4;
    public RadioButton mRDt;
    public RadioButton mRDf;

    public RadioGroup mRadioGroup;
    public RadioGroup mRadioGroupTF;

    public MaterialEditText mTextInput;


    public QuestionItemViewHolder(@NonNull View itemView) {
        super(itemView);
        mOrder = itemView.findViewById(R.id.tv_question_order);
        mScore = itemView.findViewById(R.id.tv_score);

        mRight = itemView.findViewById(R.id.tv_right);
        mWrong = itemView.findViewById(R.id.tv_wrong);
        mTitle = itemView.findViewById(R.id.tv_title);
        // 四个layout
        mMulti = itemView.findViewById(R.id.group_checkbox);
        mSingle = itemView.findViewById(R.id.group_radio);
        mChoose = itemView.findViewById(R.id.group_radio_tf);
        mFill = itemView.findViewById(R.id.group_text);

        mRadioGroup = itemView.findViewById(R.id.g_r);
        mRadioGroupTF = itemView.findViewById(R.id.g_tf);


        // 多选框
        mC1 = itemView.findViewById(R.id.ck_1);
        mC2 = itemView.findViewById(R.id.ck_2);
        mC3 = itemView.findViewById(R.id.ck_3);
        mC4 = itemView.findViewById(R.id.ck_4);
        // 单选框
        mRD1 = itemView.findViewById(R.id.rd_1);
        mRD2 = itemView.findViewById(R.id.rd_2);
        mRD3 = itemView.findViewById(R.id.rd_3);
        mRD4 = itemView.findViewById(R.id.rd_4);
        mRDt = itemView.findViewById(R.id.rd_t);
        mRDf = itemView.findViewById(R.id.rd_f);
        // 文本框
        mTextInput = itemView.findViewById(R.id.g_text);
    }
}
