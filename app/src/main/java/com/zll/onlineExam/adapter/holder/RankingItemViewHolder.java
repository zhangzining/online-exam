package com.zll.onlineExam.adapter.holder;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.zll.onlineExam.R;

/**
 * @name LearningItemViewHolder
 * @date 2022/3/8 11:35
 */
public class RankingItemViewHolder extends RecyclerView.ViewHolder {

    public TextView mNumber;
    public TextView mNickname;
    public TextView mReadCount;
    public TextView mExamCount;
    public ImageView mAvatar;
    public View view;


    public RankingItemViewHolder(@NonNull View itemView) {
        super(itemView);

        mNumber = itemView.findViewById(R.id.tv_number);
        mNickname = itemView.findViewById(R.id.tv_nickname);
        mReadCount = itemView.findViewById(R.id.tv_read_count);
        mExamCount = itemView.findViewById(R.id.tv_exam_count);
        mAvatar = itemView.findViewById(R.id.iv_avatar);
        view = itemView;
    }
}
