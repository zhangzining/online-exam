package com.zll.onlineExam.ajax;

/**
 * @name HTTPConfig
 * @date 2022/3/5 16:18
 */
public class HTTPConfig {


    public static final String HOST="http://192.168.50.102";
//    public static final String HOST="https://zll.zznzzn.cn";
    public static final String PORT=":30086";
//    public static final String PORT="";
    public static final String API=HOST+PORT+"/api/";
//    public static final String API="http://127.0.0.1:30086/api/";

    // 用户信息接口
    public static final String LOGIN="user/login";
    public static final String REGISTER="user/register";
    public static final String FlUSH="user/flush";
    public static final String CHANGE_INFO = "user/changeInfo";


    public static final String SEARCH="content/search";
    public static final String IMG_PREFIX = "uploads/";

    // 试卷接口
    public static final String LIST_PAPER = "paper/listPaper";
    public static final String LIST_PAPER_QUESTION = "paper/listPaperQuestion";
    public static final String COMMIT_EXAM = "exam/commitExam";


    public static final String SAVE_FILE = "file/saveImage";
    public static final String LIST_ARTICLE = "article/listArticle";
    public static final String LIST_READ_ARTICLE = "article/listUserReadArticle";
    public static final String READ_ARTICLE = "article/userReadArticle";
    public static final String LIST_PRODUCTION = "production/listProduction";
    public static final String LIST_USER_PRODUCTION = "production/listUserProduction";
    public static final String EXCHANGE_PRODUCTION = "production/exchangeProduction";
    public static final String LIST_USER_RANKING = "user/listUserRank";




}
