package com.zll.onlineExam.ajax;

import com.alibaba.fastjson.JSONObject;


/**
 * @name ReqCallback
 * @date 2022/3/5 16:59
 */
public interface ReqCallback {
    void onSuccess(JSONObject resp);
    void onFail(String msg);
    void onError(String msg);
}
