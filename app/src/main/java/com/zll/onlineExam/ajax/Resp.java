package com.zll.onlineExam.ajax;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @name Resp
 * @date 2022/3/5 16:35
 */
public class Resp implements Serializable {
    private String code;
    private String msg;
    private List<Map<String,String>> data;

    public String getCode() {
        return code;
    }

    public Resp setCode(String code) {
        this.code = code;
        return this;
    }

    public String getMsg() {
        return msg;
    }

    public Resp setMsg(String msg) {
        this.msg = msg;
        return this;
    }

    public List<Map<String, String>> getData() {
        return data;
    }

    public Resp setData(List<Map<String, String>> data) {
        this.data = data;
        return this;
    }

    @Override
    public String toString() {
        return "Resp{" +
                "code=" + code +
                ", msg='" + msg + '\'' +
                ", data=" + data +
                '}';
    }
}
