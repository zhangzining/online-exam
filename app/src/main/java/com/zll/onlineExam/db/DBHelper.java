package com.zll.onlineExam.db;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.zll.onlineExam.ajax.Ajax;
import com.zll.onlineExam.utils.StringUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * @name DBHelper
 * @date 2022/3/5 16:53
 */
public class DBHelper extends SQLiteOpenHelper {
    private static DBHelper instance;
    private static String TAG = "===DBHelper";

    public DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory,
                    int version) {
        super(context, "my.db", null, 1);
    }

    public static SQLiteDatabase getDB(Context context) {
        if (instance == null) {
            instance = new DBHelper(context, "my.db", null, 1);
        }
        SQLiteDatabase db = instance.getWritableDatabase();
        checkDBAndGetToken(db);
        return db;
    }

    @Override
    //数据库第一次创建时被调用
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE infos(id INTEGER PRIMARY KEY AUTOINCREMENT,name VARCHAR(50),data VARCHAR(200),remark VARCHAR(100))");
        db.execSQL("insert into infos (name,data,remark) values ('uid','','用户ID')");
        db.execSQL("insert into infos (name,data,remark) values ('nickname','','用户昵称')");
        db.execSQL("insert into infos (name,data,remark) values ('username','','用户名')");
        db.execSQL("insert into infos (name,data,remark) values ('token','','登录凭证')");
        db.execSQL("insert into infos (name,data,remark) values ('avatar','','用户头像')");
        db.execSQL("insert into infos (name,data,remark) values ('gender','','用户性别')");
        db.execSQL("insert into infos (name,data,remark) values ('score','','用户积分')");

    }

    //软件版本号发生改变时调用
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
//        db.execSQL("ALTER TABLE person ADD phone VARCHAR(12) NULL");
    }

    public static void cleanDB(SQLiteDatabase db){
        db.execSQL("drop table infos");
        db.execSQL("CREATE TABLE infos(id INTEGER PRIMARY KEY AUTOINCREMENT,name VARCHAR(50),data VARCHAR(200),remark VARCHAR(100))");
        db.execSQL("insert into infos (name,data,remark) values ('uid','','用户ID')");
        db.execSQL("insert into infos (name,data,remark) values ('nickname','','用户昵称')");
        db.execSQL("insert into infos (name,data,remark) values ('username','','用户名')");
        db.execSQL("insert into infos (name,data,remark) values ('token','','登录凭证')");
        db.execSQL("insert into infos (name,data,remark) values ('avatar','','用户头像')");
        db.execSQL("insert into infos (name,data,remark) values ('gender','','用户性别')");
        db.execSQL("insert into infos (name,data,remark) values ('score','','用户积分')");
    }

    public static String checkDBAndGetToken(SQLiteDatabase db) {
        Map<String, String> map = getDBInfos(db);
        if (map.containsKey("token") && StringUtil.isNotBlank(map.get("token"))) {
            return map.get("token");
        }
        return null;
    }

    @SuppressLint("Range")
    public static Map<String,String> getDBInfos(SQLiteDatabase db){
        Map<String, String> map = new HashMap<>();
        Cursor cursor = db.query("infos", new String[]{"name", "data", "remark"}, null, null, null, null, null);
        while (cursor.moveToNext()) {
            map.put(cursor.getString(cursor.getColumnIndex("name")), cursor.getString(cursor.getColumnIndex("data")));
        }

        for (Map.Entry<String,String> en : map.entrySet()) {
            Log.d(TAG, "checkDB: "+en.getKey()+" : "+en.getValue());
        }
        return map;
    }

    public static void initUser(SQLiteDatabase db, Map<String, String> object) {
        String[] cols = new String[]{"uid", "nickname", "username", "token", "avatar", "gender","score"};
        ContentValues cv = new ContentValues();
        for (String col : cols) {
            cv.clear();
            cv.put("data", object.get(col));
            db.update("infos", cv, "name=?", new String[]{col});
        }
    }
}