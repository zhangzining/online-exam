package com.zll.onlineExam.entity;

/**
 * @name ServiceConstant
 * @date 2022/3/5 19:00
 */
public class ServiceConstant {
    public static final int STATUS_VALID = 1;
    public static final int STATUS_INVALID = 0;

    public static final int IS_CORRECT = 1;
    public static final int IS_NOT_CORRECT = 0;

    public static final int PRIVILEGE_DISABLE = 0;
    public static final int PRIVILEGE_NORMAL = 1;
    public static final int PRIVILEGE_ADMINISTRATOR = 2;
    public static final int PRIVILEGE_ADMIN = 3;

    public static final int PRODUCTION_VISIBLE = 1;
    public static final int PRODUCTION_INVISIBLE = 2;

    public static final int ARTICLE_VISIBLE = 1;
    public static final int ARTICLE_INVISIBLE = 2;

    public static final int MESSAGE_UNREAD = 1;
    public static final int MESSAGE_READ = 2;

    public static final int MENU_TOP = 1;
    public static final int MENU_SUB = 2;


    public static final String PRODUCTION = "production";
    public static final String SUBSCRIBE = "subscribe";
    public static final String ARTICLE = "article";
    public static final String REVIEW = "review";
    public static final String ALL = "all";

    public static final String TYPE_SUBSCRIBE = "type_subscribe";
    public static final String TYPE_SYSTEM = "type_system";
    public static final String TYPE_ADMIN = "type_admin";
    public static final String TYPE_ACTIVITY = "type_activity";

    public static final String MSG_TITLE_SUBSCRIBE = "关注信息";
    public static final String MSG_TITLE_SYSTEM = "系统信息";
    public static final String MSG_TITLE_ADMIN = "管理员信息";
    public static final String MSG_TITLE_ACTIVITY = "活动信息";


    public static final String USERNAME = "Username";
    public static final String NICKNAME = "Nickname";

    public static final String SINGLE = "single";
    public static final String MULTI = "multi";
    public static final String CHOOSE = "choose";
    public static final String FILL = "fill";

}
