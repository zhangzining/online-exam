package com.zll.onlineExam.entity.dto;

import com.zll.onlineExam.entity.ServiceConstant;
import com.zll.onlineExam.entity.unused.ApiModel;
import com.zll.onlineExam.entity.unused.ApiModelProperty;

import java.util.Date;

@ApiModel("点赞表")
public class ApproveDto extends PageDto{
    @ApiModelProperty("主键id")
    private Long aid;
    @ApiModelProperty("点赞者id 32")
    private String fromUid;
    @ApiModelProperty("点赞内容id aid/pid")
    private Long toAid;
    @ApiModelProperty("类型 article/production")
    private String type;
    @ApiModelProperty("记录状态 0 失效 1 有效")
    private int status = ServiceConstant.STATUS_VALID;
    @ApiModelProperty("创建时间")
    private Date createTime;
}
