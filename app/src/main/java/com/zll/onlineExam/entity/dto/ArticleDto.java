package com.zll.onlineExam.entity.dto;

import com.zll.onlineExam.entity.ServiceConstant;
import com.zll.onlineExam.entity.unused.ApiModel;
import com.zll.onlineExam.entity.unused.ApiModelProperty;

import java.util.Date;

@ApiModel("文章表")
public class ArticleDto extends PageDto{
    @ApiModelProperty("主键id")
    private Long aid;
    @ApiModelProperty("创建者ID 32")
    private String publisher;
    @ApiModelProperty("标题 50")
    private String title;
    @ApiModelProperty("内容")
    private String content;
    @ApiModelProperty("文章类型 'article/review' 20")
    private String type;
    @ApiModelProperty("关联产品Id")
    private Long pid;
    @ApiModelProperty("标签 300")
    private String tags;
    @ApiModelProperty("记录状态 0 失效 1 可见 2 隐藏")
    private int status = ServiceConstant.ARTICLE_VISIBLE;
    @ApiModelProperty("创建时间")
    private Date createTime;
    @ApiModelProperty("奖励时间")
    private String bonusTime;
    @ApiModelProperty("奖励分数")
    private String bonusScore;
    @ApiModelProperty("封面图片")
    private String coverImage;

    public Long getAid() {
        return aid;
    }

    public ArticleDto setAid(Long aid) {
        this.aid = aid;
        return this;
    }

    public String getPublisher() {
        return publisher;
    }

    public ArticleDto setPublisher(String publisher) {
        this.publisher = publisher;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public ArticleDto setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getContent() {
        return content;
    }

    public ArticleDto setContent(String content) {
        this.content = content;
        return this;
    }

    public String getType() {
        return type;
    }

    public ArticleDto setType(String type) {
        this.type = type;
        return this;
    }

    public Long getPid() {
        return pid;
    }

    public ArticleDto setPid(Long pid) {
        this.pid = pid;
        return this;
    }

    public String getTags() {
        return tags == null?"":tags;
    }

    public ArticleDto setTags(String tags) {
        this.tags = tags;
        return this;
    }

    public int getStatus() {
        return status;
    }

    public ArticleDto setStatus(int status) {
        this.status = status;
        return this;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public ArticleDto setCreateTime(Date createTime) {
        this.createTime = createTime;
        return this;
    }

    public String getBonusTime() {
        return bonusTime;
    }

    public ArticleDto setBonusTime(String bonusTime) {
        this.bonusTime = bonusTime;
        return this;
    }

    public String getBonusScore() {
        return bonusScore;
    }

    public ArticleDto setBonusScore(String bonusScore) {
        this.bonusScore = bonusScore;
        return this;
    }

    public String getCoverImage() {
        return coverImage;
    }

    public ArticleDto setCoverImage(String coverImage) {
        this.coverImage = coverImage;
        return this;
    }
}
