package com.zll.onlineExam.entity.dto;


import com.zll.onlineExam.entity.ServiceConstant;
import com.zll.onlineExam.entity.unused.ApiModel;
import com.zll.onlineExam.entity.unused.ApiModelProperty;

import java.util.Date;

@ApiModel("评论表")
public class CommentDto extends PageDto{
    @ApiModelProperty("主键id")
    private Long cid;
    @ApiModelProperty("文章id")
    private Long aid;
    @ApiModelProperty("评论者id 32")
    private String uid;
    @ApiModelProperty("评论内容")
    private String content;
    @ApiModelProperty("评论图片")
    private String pics;
    @ApiModelProperty("评分 type=production 时使用")
    private String score;
    @ApiModelProperty("评论类型 article/production")
    private String type;
    @ApiModelProperty("记录状态 0 失效 1 有效")
    private int status = ServiceConstant.STATUS_VALID;
    @ApiModelProperty("创建时间")
    private Date createTime;
}
