package com.zll.onlineExam.entity.dto;

import com.zll.onlineExam.entity.ServiceConstant;
import com.zll.onlineExam.entity.unused.ApiModel;
import com.zll.onlineExam.entity.unused.ApiModelProperty;

import java.util.Date;

@ApiModel("常量信息")
public class ConstantDto extends PageDto{
    @ApiModelProperty("主键id")
    private Long cid;
    @ApiModelProperty("类目 40")
    private String key;
    @ApiModelProperty("类目注释 40")
    private String keyName;
    @ApiModelProperty("值 40")
    private String value;
    @ApiModelProperty("值注释 40")
    private String valueName;
    @ApiModelProperty("记录状态 0 失效 1 有效")
    private int status = ServiceConstant.STATUS_VALID;
    @ApiModelProperty("创建时间")
    private Date createTime;

}
