package com.zll.onlineExam.entity.dto;

import com.zll.onlineExam.ajax.Resp;
import com.zll.onlineExam.entity.out.ArticleDtoOut;
import com.zll.onlineExam.entity.unused.ApiModel;
import com.zll.onlineExam.entity.unused.ApiModelProperty;

@ApiModel("内容信息")
public class ContentDto extends PageDto{

    @ApiModelProperty("搜索内容")
    private String keyword;
    @ApiModelProperty("产品结果")
    private Resp articles;
    @ApiModelProperty("文章结果")
    private Resp productions;

}
