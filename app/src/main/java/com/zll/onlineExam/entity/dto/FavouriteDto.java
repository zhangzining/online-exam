package com.zll.onlineExam.entity.dto;

import com.zll.onlineExam.entity.ServiceConstant;
import com.zll.onlineExam.entity.unused.ApiModel;
import com.zll.onlineExam.entity.unused.ApiModelProperty;

import java.util.Date;

@ApiModel("收藏表")
public class FavouriteDto extends PageDto{
    @ApiModelProperty("主键id")
    private Long fid;
    @ApiModelProperty("收藏者id 32")
    private String fromUid;
    @ApiModelProperty("收藏内容id aid/pid")
    private Long toAid;
    @ApiModelProperty("类型 article/production")
    private String type;
    @ApiModelProperty("记录状态 0 失效 1 有效")
    private int status = ServiceConstant.STATUS_VALID;
    @ApiModelProperty("创建时间")
    private Date createTime;
}
