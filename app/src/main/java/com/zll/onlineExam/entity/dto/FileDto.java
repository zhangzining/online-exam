package com.zll.onlineExam.entity.dto;


import com.zll.onlineExam.entity.ServiceConstant;
import com.zll.onlineExam.entity.unused.ApiModel;
import com.zll.onlineExam.entity.unused.ApiModelProperty;

import java.util.Date;

@ApiModel("文件表")
public class FileDto extends PageDto{
    @ApiModelProperty("主键id")
    private Long id;
    @ApiModelProperty("文件MD5 40")
    private String fileMd5;
    @ApiModelProperty("保存的文件名 40")
    private String fileName;
    @ApiModelProperty("保存的文件扩展名 10")
    private String fileType;
    @ApiModelProperty("记录状态 0 失效 1 有效")
    private int status = ServiceConstant.STATUS_VALID;
    @ApiModelProperty("创建时间")
    private Date createTime;

}
