package com.zll.onlineExam.entity.dto;

import com.zll.onlineExam.entity.ServiceConstant;
import com.zll.onlineExam.entity.unused.ApiModel;
import com.zll.onlineExam.entity.unused.ApiModelProperty;

import java.util.Date;

@ApiModel("消息表")
public class MessageDto extends PageDto {
    /**
     * 在群发情境下，无法通过为每一个用户生成一条记录的方式记录已读状态
     * 故采用分表处理方式，
     * 一张表记录消息的发布信息，
     * 一张表记录已读的消息，减少冗余
     * 一张表记录已删除的消息，减少冗余
     */
    @ApiModelProperty("主键id")
    private Long mid;
    @ApiModelProperty("发送者ID 32")
    private String fromUid;
    @ApiModelProperty("接受者ID 32")
    private String toUid;
    @ApiModelProperty("标题")
    private String title;
    @ApiModelProperty("消息内容")
    private String content;
    @ApiModelProperty("消息类型 关注消息 系统消息 管理员消息 活动消息")
    private String type;
    @ApiModelProperty("记录状态 0 失效 1 未读 2 已读 3 保存未发布")
    private int status = ServiceConstant.MESSAGE_UNREAD;
    @ApiModelProperty("创建时间")
    private Date createTime;

}
