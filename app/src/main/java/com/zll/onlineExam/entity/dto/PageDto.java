package com.zll.onlineExam.entity.dto;


import com.zll.onlineExam.entity.unused.ApiModel;
import com.zll.onlineExam.entity.unused.ApiModelProperty;

import java.io.Serializable;

@ApiModel("分页dto")
public abstract class PageDto implements Serializable {
    public static final int DEFAULT_PAGE = -1;

    @ApiModelProperty("分页所在页数")
    private int page = DEFAULT_PAGE;
    @ApiModelProperty("每页行数")
    private int row;
    @ApiModelProperty("总记录数")
    private int total;
    @ApiModelProperty("分页后页数")
    private int records;
}
