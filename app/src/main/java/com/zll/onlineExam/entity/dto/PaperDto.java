package com.zll.onlineExam.entity.dto;

import com.zll.onlineExam.entity.ServiceConstant;
import com.zll.onlineExam.entity.unused.ApiModel;
import com.zll.onlineExam.entity.unused.ApiModelProperty;

import java.util.Date;

@ApiModel("试卷表")
public class PaperDto extends PageDto{

    @ApiModelProperty("主键id")
    private Long pid;
    @ApiModelProperty("试卷标题")
    private String title;
    @ApiModelProperty("试卷简介")
    private String summary;
    @ApiModelProperty("试卷类型 limited unlimited")
    private String type;
    @ApiModelProperty("测试时长 分钟")
    private String duration;
    @ApiModelProperty("过期时间")
    private String expireTime;
    @ApiModelProperty("试卷标签")
    private String tag;
    @ApiModelProperty("记录状态 0 失效 1 有效")
    private int status = ServiceConstant.STATUS_VALID;
    @ApiModelProperty("创建时间")
    private Date createTime;

    public Long getPid() {
        return pid;
    }

    public PaperDto setPid(Long pid) {
        this.pid = pid;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public PaperDto setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getSummary() {
        return summary;
    }

    public PaperDto setSummary(String summary) {
        this.summary = summary;
        return this;
    }

    public String getType() {
        return type;
    }

    public PaperDto setType(String type) {
        this.type = type;
        return this;
    }

    public String getDuration() {
        return duration;
    }

    public PaperDto setDuration(String duration) {
        this.duration = duration;
        return this;
    }

    public String getExpireTime() {
        return expireTime;
    }

    public PaperDto setExpireTime(String expireTime) {
        this.expireTime = expireTime;
        return this;
    }

    public String getTag() {
        return tag;
    }

    public PaperDto setTag(String tag) {
        this.tag = tag;
        return this;
    }

    public int getStatus() {
        return status;
    }

    public PaperDto setStatus(int status) {
        this.status = status;
        return this;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public PaperDto setCreateTime(Date createTime) {
        this.createTime = createTime;
        return this;
    }
}
