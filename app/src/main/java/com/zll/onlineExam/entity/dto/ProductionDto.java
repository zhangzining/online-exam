package com.zll.onlineExam.entity.dto;

import com.zll.onlineExam.entity.ServiceConstant;
import com.zll.onlineExam.entity.unused.ApiModel;
import com.zll.onlineExam.entity.unused.ApiModelProperty;

import java.util.Date;

@ApiModel("产品表")
public class ProductionDto extends PageDto{
    @ApiModelProperty("主键id")
    private Long pid;
    @ApiModelProperty("产品名称 40")
    private String title;
    @ApiModelProperty("品牌 40")
    private String brand;
    @ApiModelProperty("产品类型 20")
    private String type;
    @ApiModelProperty("产品规格 30")
    private String size;
    @ApiModelProperty("产品参考价格 20")
    private String price;
    @ApiModelProperty("产品主要描述 50")
    private String usage;
    @ApiModelProperty("产品图片")
    private String pictures;
    @ApiModelProperty("记录状态 0 失效 1 可见 2 隐藏")
    private int status = ServiceConstant.PRODUCTION_VISIBLE;
    @ApiModelProperty("创建时间")
    private Date createTime;
    @ApiModelProperty("奖励分数")
    private String bonusScore;
    @ApiModelProperty("库存")
    private Integer stock;

    public Long getPid() {
        return pid;
    }

    public ProductionDto setPid(Long pid) {
        this.pid = pid;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public ProductionDto setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getBrand() {
        return brand;
    }

    public ProductionDto setBrand(String brand) {
        this.brand = brand;
        return this;
    }

    public String getType() {
        return type;
    }

    public ProductionDto setType(String type) {
        this.type = type;
        return this;
    }

    public String getSize() {
        return size;
    }

    public ProductionDto setSize(String size) {
        this.size = size;
        return this;
    }

    public String getPrice() {
        return price;
    }

    public ProductionDto setPrice(String price) {
        this.price = price;
        return this;
    }

    public String getUsage() {
        return usage;
    }

    public ProductionDto setUsage(String usage) {
        this.usage = usage;
        return this;
    }

    public String getPictures() {
        return pictures;
    }

    public ProductionDto setPictures(String pictures) {
        this.pictures = pictures;
        return this;
    }

    public int getStatus() {
        return status;
    }

    public ProductionDto setStatus(int status) {
        this.status = status;
        return this;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public ProductionDto setCreateTime(Date createTime) {
        this.createTime = createTime;
        return this;
    }

    public String getBonusScore() {
        return bonusScore;
    }

    public ProductionDto setBonusScore(String bonusScore) {
        this.bonusScore = bonusScore;
        return this;
    }

    public Integer getStock() {
        return stock;
    }

    public ProductionDto setStock(Integer stock) {
        this.stock = stock;
        return this;
    }
}
