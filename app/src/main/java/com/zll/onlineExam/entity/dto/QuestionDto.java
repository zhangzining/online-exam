package com.zll.onlineExam.entity.dto;


import com.zll.onlineExam.entity.ServiceConstant;
import com.zll.onlineExam.entity.unused.ApiModel;
import com.zll.onlineExam.entity.unused.ApiModelProperty;

import java.util.Date;

@ApiModel("题目表")
public class QuestionDto extends PageDto{

    @ApiModelProperty("主键id")
    private Long qid;
    @ApiModelProperty("试卷id")
    private Long pid;
    @ApiModelProperty("题目标题")
    private String title;
    @ApiModelProperty("题目分值")
    private String score;
    @ApiModelProperty("题目类型 关联constant表")
    private String type;
    @ApiModelProperty("选项1")
    private String option1;
    @ApiModelProperty("选项2")
    private String option2;
    @ApiModelProperty("选项3")
    private String option3;
    @ApiModelProperty("选项4")
    private String option4;
    @ApiModelProperty("答案 用,分割")
    private String answers;
    @ApiModelProperty("记录状态 0 失效 1 有效")
    private int status = ServiceConstant.STATUS_VALID;
    @ApiModelProperty("创建时间")
    private Date createTime;

    public Long getQid() {
        return qid;
    }

    public QuestionDto setQid(Long qid) {
        this.qid = qid;
        return this;
    }

    public Long getPid() {
        return pid;
    }

    public QuestionDto setPid(Long pid) {
        this.pid = pid;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public QuestionDto setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getScore() {
        return score;
    }

    public QuestionDto setScore(String score) {
        this.score = score;
        return this;
    }

    public String getType() {
        return type;
    }

    public QuestionDto setType(String type) {
        this.type = type;
        return this;
    }

    public String getOption1() {
        return option1;
    }

    public QuestionDto setOption1(String option1) {
        this.option1 = option1;
        return this;
    }

    public String getOption2() {
        return option2;
    }

    public QuestionDto setOption2(String option2) {
        this.option2 = option2;
        return this;
    }

    public String getOption3() {
        return option3;
    }

    public QuestionDto setOption3(String option3) {
        this.option3 = option3;
        return this;
    }

    public String getOption4() {
        return option4;
    }

    public QuestionDto setOption4(String option4) {
        this.option4 = option4;
        return this;
    }

    public String getAnswers() {
        return answers;
    }

    public QuestionDto setAnswers(String answers) {
        this.answers = answers;
        return this;
    }

    public int getStatus() {
        return status;
    }

    public QuestionDto setStatus(int status) {
        this.status = status;
        return this;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public QuestionDto setCreateTime(Date createTime) {
        this.createTime = createTime;
        return this;
    }
}
