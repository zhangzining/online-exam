package com.zll.onlineExam.entity.dto;


import com.zll.onlineExam.entity.unused.ApiModelProperty;

import java.util.Date;

public class ReadMessageDto extends PageDto{
    @ApiModelProperty("主键id")
    private Long rid;
    @ApiModelProperty("消息表主键id")
    private Long mid;
    @ApiModelProperty("发送者ID 32")
    private String fromUid;
    @ApiModelProperty("接受者ID 32")
    private String toUid;
    @ApiModelProperty("创建时间/已读时间")
    private Date createTime;
}
