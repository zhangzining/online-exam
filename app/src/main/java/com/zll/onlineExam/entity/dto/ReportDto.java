package com.zll.onlineExam.entity.dto;


import com.zll.onlineExam.entity.ServiceConstant;
import com.zll.onlineExam.entity.unused.ApiModel;
import com.zll.onlineExam.entity.unused.ApiModelProperty;

import java.util.Date;
@ApiModel("投诉表")
public class ReportDto extends PageDto{
    @ApiModelProperty("主键id")
    private Long rid;
    @ApiModelProperty("投诉人id")
    private String fromUid;
    @ApiModelProperty("内容id")
    private String outId;
    @ApiModelProperty("内容类型")
    private String type;
    @ApiModelProperty("投诉理由")
    private String reason;
    @ApiModelProperty("投诉内容")
    private String content;
    @ApiModelProperty("图片")
    private String pics;
    @ApiModelProperty("处理意见")
    private String result;
    @ApiModelProperty("处理状态 0 新建 1 驳回 2 已处理")
    private String process;
    @ApiModelProperty("记录状态 0 失效 1 有效")
    private int status = ServiceConstant.STATUS_VALID;
    @ApiModelProperty("创建时间")
    private Date createTime;
}
