package com.zll.onlineExam.entity.dto;


import com.zll.onlineExam.entity.ServiceConstant;
import com.zll.onlineExam.entity.unused.ApiModel;
import com.zll.onlineExam.entity.unused.ApiModelProperty;

import java.util.Date;

@ApiModel("关注表")
public class SubscribeDto extends PageDto{
    @ApiModelProperty("主键id")
    private Long sid;
    @ApiModelProperty("关注者id 32")
    private String fromUid;
    @ApiModelProperty("被关注者id 32")
    private String toUid;
    @ApiModelProperty("记录状态 0 失效 1 有效")
    private int status = ServiceConstant.STATUS_VALID;
    @ApiModelProperty("创建时间")
    private Date createTime;
}
