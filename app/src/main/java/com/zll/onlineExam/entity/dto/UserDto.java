package com.zll.onlineExam.entity.dto;

import com.zll.onlineExam.entity.ServiceConstant;
import com.zll.onlineExam.entity.unused.ApiModel;
import com.zll.onlineExam.entity.unused.ApiModelProperty;

import java.util.Date;

@ApiModel("用户信息")
public class UserDto extends PageDto{
    @ApiModelProperty("主键id")
    private Long id;
    @ApiModelProperty("用户id 32")
    private String uid;
    @ApiModelProperty("用户登录名 30")
    private String username;
    @ApiModelProperty("用户昵称 20")
    private String nickname;
    @ApiModelProperty("加密后的密码 64")
    private String password;
    @ApiModelProperty("加密用的盐 32")
    private String salt;
    @ApiModelProperty("头像文件名 40")
    private String avatar;

    @ApiModelProperty("生日")
    private Date birthday;
    @ApiModelProperty("性别 0男 1女")
    private String gender;
    @ApiModelProperty("邮箱 40")
    private String email;
    @ApiModelProperty("手机号 20")
    private String phone;

    @ApiModelProperty("token 250")
    private String token;
    @ApiModelProperty("个性签名 50")
    private String description;
    @ApiModelProperty("用户肤质 30")
    private String skinType;
    @ApiModelProperty("用户积分")
    private String score;
    @ApiModelProperty("总积分")
    private String totalScore;

    @ApiModelProperty("用户权限 0 禁用 1 普通 2 超管 3 admin")
    private Integer privilege = ServiceConstant.PRIVILEGE_NORMAL;
    @ApiModelProperty("记录状态 0 失效 1 有效")
    private int status = ServiceConstant.STATUS_VALID;
    @ApiModelProperty("创建时间")
    private Date createTime;


    public String getTotalScore() {
        return totalScore;
    }

    public UserDto setTotalScore(String totalScore) {
        this.totalScore = totalScore;
        return this;
    }

    public Long getId() {
        return id;
    }

    public UserDto setId(Long id) {
        this.id = id;
        return this;
    }

    public String getUid() {
        return uid;
    }

    public UserDto setUid(String uid) {
        this.uid = uid;
        return this;
    }

    public String getUsername() {
        return username;
    }

    public UserDto setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getNickname() {
        return nickname;
    }

    public UserDto setNickname(String nickname) {
        this.nickname = nickname;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public UserDto setPassword(String password) {
        this.password = password;
        return this;
    }

    public String getSalt() {
        return salt;
    }

    public UserDto setSalt(String salt) {
        this.salt = salt;
        return this;
    }

    public String getAvatar(String prefix) {
        return prefix+avatar;
    }

    public UserDto setAvatar(String avatar) {
        this.avatar = avatar;
        return this;
    }

    public Date getBirthday() {
        return birthday;
    }

    public UserDto setBirthday(Date birthday) {
        this.birthday = birthday;
        return this;
    }

    public String getGender() {
        return gender;
    }

    public UserDto setGender(String gender) {
        this.gender = gender;
        return this;
    }

    public String getEmail() {
        return email == null?"":email;
    }

    public UserDto setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getPhone() {
        return phone;
    }

    public UserDto setPhone(String phone) {
        this.phone = phone;
        return this;
    }

    public String getToken() {
        return token;
    }

    public UserDto setToken(String token) {
        this.token = token;
        return this;
    }

    public String getDescription() {
        return description == null?"":description;
    }

    public UserDto setDescription(String description) {
        this.description = description;
        return this;
    }

    public String getSkinType() {
        return skinType;
    }

    public UserDto setSkinType(String skinType) {
        this.skinType = skinType;
        return this;
    }

    public Integer getPrivilege() {
        return privilege;
    }

    public UserDto setPrivilege(Integer privilege) {
        this.privilege = privilege;
        return this;
    }

    public int getStatus() {
        return status;
    }

    public UserDto setStatus(int status) {
        this.status = status;
        return this;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public UserDto setCreateTime(Date createTime) {
        this.createTime = createTime;
        return this;
    }

    public void harmless(){
        this.password = null;
        this.salt = null;
    }

    public String getAvatar() {
        return avatar;
    }

    public String getScore() {
        return score;
    }

    public UserDto setScore(String score) {
        this.score = score;
        return this;
    }
}
