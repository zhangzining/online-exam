package com.zll.onlineExam.entity.in;

import com.zll.onlineExam.entity.dto.ApproveDto;
import com.zll.onlineExam.entity.unused.ApiModel;

@ApiModel("点赞IN")
public class ApproveDtoIn extends ApproveDto {
}
