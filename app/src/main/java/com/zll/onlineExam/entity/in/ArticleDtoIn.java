package com.zll.onlineExam.entity.in;

import com.zll.onlineExam.entity.dto.ArticleDto;
import com.zll.onlineExam.entity.unused.ApiModelProperty;

public class ArticleDtoIn extends ArticleDto {
    @ApiModelProperty("当前登录用户")
    private String currentUid;
}
