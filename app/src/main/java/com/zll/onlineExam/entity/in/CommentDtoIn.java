package com.zll.onlineExam.entity.in;

import com.zll.onlineExam.entity.dto.CommentDto;
import com.zll.onlineExam.entity.unused.ApiModelProperty;

public class CommentDtoIn extends CommentDto {
    @ApiModelProperty("当前登录用户")
    private String currentUid;
}
