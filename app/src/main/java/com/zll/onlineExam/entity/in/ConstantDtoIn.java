package com.zll.onlineExam.entity.in;

import com.zll.onlineExam.entity.dto.ConstantDto;
import com.zll.onlineExam.entity.unused.ApiModel;
import com.zll.onlineExam.entity.unused.ApiModelProperty;

@ApiModel("常量表IN")
public class ConstantDtoIn extends ConstantDto {
    @ApiModelProperty("新键")
    private String newKey;
}
