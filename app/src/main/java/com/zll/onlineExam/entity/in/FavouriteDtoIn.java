package com.zll.onlineExam.entity.in;

import com.zll.onlineExam.entity.dto.FavouriteDto;
import com.zll.onlineExam.entity.unused.ApiModel;

@ApiModel("收藏IN")
public class FavouriteDtoIn extends FavouriteDto {
}
