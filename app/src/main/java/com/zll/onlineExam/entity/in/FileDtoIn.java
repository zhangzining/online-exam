package com.zll.onlineExam.entity.in;

import com.zll.onlineExam.entity.dto.FileDto;
import com.zll.onlineExam.entity.unused.ApiModel;
import com.zll.onlineExam.entity.unused.ApiModelProperty;

import java.util.List;

@ApiModel("用户信息IN")
public class FileDtoIn extends FileDto {
    @ApiModelProperty("以base64编码的文件")
    private String base64File;
    @ApiModelProperty("以base64编码的文件s")
    private List<String> base64Files;
    @ApiModelProperty("要删除的文件ids")
    private List<Integer> fileIds;
}
