package com.zll.onlineExam.entity.in;

import com.zll.onlineExam.entity.dto.MessageDto;
import com.zll.onlineExam.entity.unused.ApiModel;
import com.zll.onlineExam.entity.unused.ApiModelProperty;

import java.util.List;

@ApiModel("消息表IN")
public class MessageDtoIn extends MessageDto {

    @ApiModelProperty("消息记录主键数组")
    private List<Long> midList;
    @ApiModelProperty("接收者ID")
    private String receiverUid;
    @ApiModelProperty("另一个可选状态")
    private String sideStatus;

}
