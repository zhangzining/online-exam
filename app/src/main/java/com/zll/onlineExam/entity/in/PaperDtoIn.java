package com.zll.onlineExam.entity.in;


import com.zll.onlineExam.entity.dto.PaperDto;
import com.zll.onlineExam.entity.unused.ApiModel;
import com.zll.onlineExam.entity.unused.ApiModelProperty;

import java.util.List;

@ApiModel("试卷表IN")
public class PaperDtoIn extends PaperDto {
    @ApiModelProperty("设计的题目")
    private List<QuestionDtoIn> questions;
//    @ApiModelProperty("用户做的答案")
//    private List<UserQuestionAnswerDtoIn> userAnswers;
    @ApiModelProperty("做题的用户")
    private String uid;
    @ApiModelProperty("做题的时长")
    private String costTime;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }
}
