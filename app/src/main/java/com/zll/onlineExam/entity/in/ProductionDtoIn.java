package com.zll.onlineExam.entity.in;

import com.zll.onlineExam.entity.dto.ProductionDto;
import com.zll.onlineExam.entity.unused.ApiModel;
import com.zll.onlineExam.entity.unused.ApiModelProperty;

import java.util.List;

@ApiModel("产品表IN")
public class ProductionDtoIn extends ProductionDto {
    @ApiModelProperty("查询符合肤质的产品")
    private List<String> fitSkins;
    @ApiModelProperty("当前登录用户")
    private String currentUid;
}
