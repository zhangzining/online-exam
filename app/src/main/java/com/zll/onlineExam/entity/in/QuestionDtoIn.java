package com.zll.onlineExam.entity.in;

import com.zll.onlineExam.entity.dto.QuestionDto;
import com.zll.onlineExam.entity.unused.ApiModel;

@ApiModel("题目表IN")
public class QuestionDtoIn extends QuestionDto {
}
