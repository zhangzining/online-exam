package com.zll.onlineExam.entity.in;

import com.zll.onlineExam.entity.dto.SubscribeDto;
import com.zll.onlineExam.entity.unused.ApiModel;

@ApiModel("关注IN")
public class SubscribeDtoIn extends SubscribeDto {
}
