package com.zll.onlineExam.entity.in;

import com.zll.onlineExam.entity.dto.UserDto;
import com.zll.onlineExam.entity.unused.ApiModel;
import com.zll.onlineExam.entity.unused.ApiModelProperty;

@ApiModel("用户信息IN")
public class UserDtoIn extends UserDto {

    @ApiModelProperty("是否记住登录")
    private boolean remember;
    @ApiModelProperty("验证码")
    private String verifyCode;
    @ApiModelProperty("关注者ID")
    private String fromUid;

}
