package com.zll.onlineExam.entity.out;

import com.zll.onlineExam.entity.dto.ArticleDto;
import com.zll.onlineExam.entity.unused.ApiModel;
import com.zll.onlineExam.entity.unused.ApiModelProperty;

@ApiModel("文章详情OUT")
public class ArticleDtoOut extends ArticleDto {
    @ApiModelProperty("发布者")
    private UserDtoOut publisherUser;
    @ApiModelProperty("点赞状态")
    private Integer approved;
    @ApiModelProperty("点赞数")
    private Integer approveCount;
    @ApiModelProperty("收藏状态")
    private Integer favourite;
    @ApiModelProperty("收藏数")
    private Integer favouriteCount;
    @ApiModelProperty("是否关注")
    private Integer subscribed;
    @ApiModelProperty("评论数")
    private Integer commentCount;
    @ApiModelProperty("获得积分")
    private Integer gainBonusScore;

    public UserDtoOut getPublisherUser() {
        return publisherUser;
    }

    public ArticleDtoOut setPublisherUser(UserDtoOut publisherUser) {
        this.publisherUser = publisherUser;
        return this;
    }

    public Integer getApproved() {
        return approved;
    }

    public ArticleDtoOut setApproved(Integer approved) {
        this.approved = approved;
        return this;
    }

    public Integer getApproveCount() {
        return approveCount;
    }

    public ArticleDtoOut setApproveCount(Integer approveCount) {
        this.approveCount = approveCount;
        return this;
    }

    public Integer getFavourite() {
        return favourite;
    }

    public ArticleDtoOut setFavourite(Integer favourite) {
        this.favourite = favourite;
        return this;
    }

    public Integer getFavouriteCount() {
        return favouriteCount;
    }

    public ArticleDtoOut setFavouriteCount(Integer favouriteCount) {
        this.favouriteCount = favouriteCount;
        return this;
    }

    public Integer getSubscribed() {
        return subscribed;
    }

    public ArticleDtoOut setSubscribed(Integer subscribed) {
        this.subscribed = subscribed;
        return this;
    }

    public Integer getCommentCount() {
        return commentCount;
    }

    public ArticleDtoOut setCommentCount(Integer commentCount) {
        this.commentCount = commentCount;
        return this;
    }

    public Integer getGainBonusScore() {
        return gainBonusScore;
    }

    public ArticleDtoOut setGainBonusScore(Integer gainBonusScore) {
        this.gainBonusScore = gainBonusScore;
        return this;
    }
}
