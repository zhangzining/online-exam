package com.zll.onlineExam.entity.out;

import com.zll.onlineExam.entity.dto.CommentDto;
import com.zll.onlineExam.entity.dto.UserDto;
import com.zll.onlineExam.entity.unused.ApiModel;
import com.zll.onlineExam.entity.unused.ApiModelProperty;

@ApiModel("评论表OUT")
public class CommentDtoOut extends CommentDto {
    @ApiModelProperty("评论者信息")
    private UserDto user;
    @ApiModelProperty("点赞数")
    private Integer approveCount;
    @ApiModelProperty("当前用户是否已点赞")
    private Integer approved;
}
