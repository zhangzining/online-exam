package com.zll.onlineExam.entity.out;

import com.zll.onlineExam.entity.dto.ConstantDto;
import com.zll.onlineExam.entity.unused.ApiModel;
import com.zll.onlineExam.entity.unused.ApiModelProperty;

import java.util.List;

@ApiModel("常量表OUT")
public class ConstantDtoOut extends ConstantDto {

    @ApiModelProperty("值数组")
    private List<ConstantDto> values;
}
