package com.zll.onlineExam.entity.out;


import com.zll.onlineExam.entity.dto.FavouriteDto;
import com.zll.onlineExam.entity.unused.ApiModel;
import com.zll.onlineExam.entity.unused.ApiModelProperty;

import java.util.List;

@ApiModel("收藏OUT")
public class FavouriteDtoOut extends FavouriteDto {

    @ApiModelProperty("收藏的商品")
    List<ProductionDtoOut> productions;
    @ApiModelProperty("收藏的文章")
    List<ArticleDtoOut> articles;

}
