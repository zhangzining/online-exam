package com.zll.onlineExam.entity.out;

import com.zll.onlineExam.entity.dto.MessageDto;
import com.zll.onlineExam.entity.unused.ApiModel;
import com.zll.onlineExam.entity.unused.ApiModelProperty;

@ApiModel("消息表OUT")
public class MessageDtoOut extends MessageDto {
    @ApiModelProperty("发送者昵称")
    private String fromName;
    @ApiModelProperty("接收者昵称")
    private String toName;
    @ApiModelProperty("已读状态")
    private Integer readStatus;
}
