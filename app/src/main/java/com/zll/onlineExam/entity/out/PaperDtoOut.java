package com.zll.onlineExam.entity.out;

import com.zll.onlineExam.entity.dto.PaperDto;
import com.zll.onlineExam.entity.unused.ApiModel;
import com.zll.onlineExam.entity.unused.ApiModelProperty;

import java.util.List;

@ApiModel("试卷表OUT")
public class PaperDtoOut extends PaperDto {
    @ApiModelProperty("题目")
    private List<QuestionDtoOut> questions;
    @ApiModelProperty("题目数量")
    private Integer questionCount;
    @ApiModelProperty("题目总分")
    private Double totalScore;
    @ApiModelProperty("答对题数")
    private Integer userCorrectCount;
    @ApiModelProperty("所用时长")
    private String userCostTime;
    @ApiModelProperty("总成绩")
    private String userTotalScore;
    @ApiModelProperty("答题者")
    private String committerId;
    @ApiModelProperty("答题者")
    private String committerName;

    public String getCommitterId() {
        return committerId;
    }

    public PaperDtoOut setCommitterId(String committerId) {
        this.committerId = committerId;
        return this;
    }

    public String getCommitterName() {
        return committerName;
    }

    public PaperDtoOut setCommitterName(String committerName) {
        this.committerName = committerName;
        return this;
    }

    public List<QuestionDtoOut> getQuestions() {
        return questions;
    }

    public PaperDtoOut setQuestions(List<QuestionDtoOut> questions) {
        this.questions = questions;
        return this;
    }

    public Integer getQuestionCount() {
        return questionCount;
    }

    public PaperDtoOut setQuestionCount(Integer questionCount) {
        this.questionCount = questionCount;
        return this;
    }

    public Double getTotalScore() {
        return totalScore;
    }

    public PaperDtoOut setTotalScore(Double totalScore) {
        this.totalScore = totalScore;
        return this;
    }

    public Integer getUserCorrectCount() {
        return userCorrectCount;
    }

    public PaperDtoOut setUserCorrectCount(Integer userCorrectCount) {
        this.userCorrectCount = userCorrectCount;
        return this;
    }

    public String getUserCostTime() {
        return userCostTime;
    }

    public PaperDtoOut setUserCostTime(String userCostTime) {
        this.userCostTime = userCostTime;
        return this;
    }

    public String getUserTotalScore() {
        return userTotalScore;
    }

    public PaperDtoOut setUserTotalScore(String userTotalScore) {
        this.userTotalScore = userTotalScore;
        return this;
    }
}
