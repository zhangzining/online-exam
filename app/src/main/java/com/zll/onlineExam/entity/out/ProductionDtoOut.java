package com.zll.onlineExam.entity.out;

import com.zll.onlineExam.entity.dto.ProductionDto;
import com.zll.onlineExam.entity.unused.ApiModel;
import com.zll.onlineExam.entity.unused.ApiModelProperty;

import java.util.List;

@ApiModel("产品表OUT")
public class ProductionDtoOut extends ProductionDto {
    @ApiModelProperty("产品图片")
    private List<String> pictureList;
    @ApiModelProperty("品牌中文名")
    private String brandName;

    @ApiModelProperty("收藏状态")
    private Integer favourite;
    @ApiModelProperty("收藏数")
    private Integer favouriteCount;
    @ApiModelProperty("平均分")
    private String avgScore;
    @ApiModelProperty("兑换花费积分")
    private Integer exchangeScore;

    public List<String> getPictureList() {
        return pictureList;
    }

    public ProductionDtoOut setPictureList(List<String> pictureList) {
        this.pictureList = pictureList;
        return this;
    }

    public String getBrandName() {
        return brandName;
    }

    public ProductionDtoOut setBrandName(String brandName) {
        this.brandName = brandName;
        return this;
    }

    public Integer getFavourite() {
        return favourite;
    }

    public ProductionDtoOut setFavourite(Integer favourite) {
        this.favourite = favourite;
        return this;
    }

    public Integer getFavouriteCount() {
        return favouriteCount;
    }

    public ProductionDtoOut setFavouriteCount(Integer favouriteCount) {
        this.favouriteCount = favouriteCount;
        return this;
    }

    public String getAvgScore() {
        return avgScore;
    }

    public ProductionDtoOut setAvgScore(String avgScore) {
        this.avgScore = avgScore;
        return this;
    }

    public Integer getExchangeScore() {
        return exchangeScore;
    }

    public ProductionDtoOut setExchangeScore(Integer exchangeScore) {
        this.exchangeScore = exchangeScore;
        return this;
    }
}
