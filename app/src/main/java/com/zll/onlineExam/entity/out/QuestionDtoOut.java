package com.zll.onlineExam.entity.out;

import com.zll.onlineExam.entity.dto.QuestionDto;
import com.zll.onlineExam.entity.unused.ApiModel;
import com.zll.onlineExam.entity.unused.ApiModelProperty;

@ApiModel("题目表OUT")
public class QuestionDtoOut extends QuestionDto {

    @ApiModelProperty("用户答案 用,分割")
    private String userAnswers;
    @ApiModelProperty("是否正确 0 错误 1 正确")
    private Integer isCorrect;

    public String getUserAnswers() {
        return userAnswers;
    }

    public QuestionDtoOut setUserAnswers(String userAnswers) {
        this.userAnswers = userAnswers;
        return this;
    }

    public Integer getIsCorrect() {
        return isCorrect;
    }

    public QuestionDtoOut setIsCorrect(Integer isCorrect) {
        this.isCorrect = isCorrect;
        return this;
    }
}
