package com.zll.onlineExam.entity.out;


import com.zll.onlineExam.entity.dto.ReportDto;
import com.zll.onlineExam.entity.unused.ApiModel;
import com.zll.onlineExam.entity.unused.ApiModelProperty;

@ApiModel("投诉OUT")
public class ReportDtoOut extends ReportDto {
    @ApiModelProperty("用户昵称")
    private String nickname;
    @ApiModelProperty("投诉类型")
    private String reasonValue;
}
