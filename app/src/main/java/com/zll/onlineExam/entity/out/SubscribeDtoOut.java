package com.zll.onlineExam.entity.out;

import com.zll.onlineExam.entity.dto.SubscribeDto;
import com.zll.onlineExam.entity.unused.ApiModel;

@ApiModel("关注OUT")
public class SubscribeDtoOut extends SubscribeDto {
}
