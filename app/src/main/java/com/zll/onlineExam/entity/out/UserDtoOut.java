package com.zll.onlineExam.entity.out;

import com.zll.onlineExam.entity.dto.UserDto;
import com.zll.onlineExam.entity.unused.ApiModel;
import com.zll.onlineExam.entity.unused.ApiModelProperty;

@ApiModel("用户信息OUT")
public class UserDtoOut extends UserDto {
    @ApiModelProperty("当前用户是否关注了该用户")
    private Integer subscribed;
    @ApiModelProperty("关注数")
    private Integer subscribeCount;
    @ApiModelProperty("被关注数")
    private Integer subscriberCount;
    @ApiModelProperty("文章数")
    private Integer articleCount;

    @ApiModelProperty("已读文章数")
    private Integer readCount;
    @ApiModelProperty("已做测试数")
    private Integer examCount;

    public Integer getSubscribed() {
        return subscribed;
    }

    public UserDtoOut setSubscribed(Integer subscribed) {
        this.subscribed = subscribed;
        return this;
    }

    public Integer getSubscribeCount() {
        return subscribeCount;
    }

    public UserDtoOut setSubscribeCount(Integer subscribeCount) {
        this.subscribeCount = subscribeCount;
        return this;
    }

    public Integer getSubscriberCount() {
        return subscriberCount;
    }

    public UserDtoOut setSubscriberCount(Integer subscriberCount) {
        this.subscriberCount = subscriberCount;
        return this;
    }

    public Integer getArticleCount() {
        return articleCount;
    }

    public UserDtoOut setArticleCount(Integer articleCount) {
        this.articleCount = articleCount;
        return this;
    }

    public Integer getReadCount() {
        return readCount;
    }

    public UserDtoOut setReadCount(Integer readCount) {
        this.readCount = readCount;
        return this;
    }

    public Integer getExamCount() {
        return examCount;
    }

    public UserDtoOut setExamCount(Integer examCount) {
        this.examCount = examCount;
        return this;
    }
}
