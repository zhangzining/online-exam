package com.zll.onlineExam.entity.unused;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

/**
 * @name ApiModel
 * @date 2022/3/5 18:59
 */
@Target(ElementType.TYPE)
public @interface ApiModel {
    String value();
}
