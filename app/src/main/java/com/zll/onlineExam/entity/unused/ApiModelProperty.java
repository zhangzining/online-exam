package com.zll.onlineExam.entity.unused;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

/**
 * @name ApiModelProperty
 * @date 2022/3/5 18:58
 */
@Target(ElementType.FIELD)
public @interface ApiModelProperty {
    String value();
}
