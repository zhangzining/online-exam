package com.zll.onlineExam.fragments;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.xuexiang.xui.widget.imageview.RadiusImageView;
import com.xuexiang.xui.widget.textview.supertextview.SuperTextView;
import com.zll.onlineExam.R;
import com.zll.onlineExam.activities.AccountSettingActivity;
import com.zll.onlineExam.activities.BeforeLoginActivity;
import com.zll.onlineExam.activities.ExchangeHistoryActivity;
import com.zll.onlineExam.activities.LearningHistoryActivity;
import com.zll.onlineExam.activities.PasswordActivity;
import com.zll.onlineExam.activities.RankingActivity;
import com.zll.onlineExam.ajax.Ajax;
import com.zll.onlineExam.ajax.HTTPConfig;
import com.zll.onlineExam.db.DBHelper;
import com.zll.onlineExam.utils.GlobalDataHolder;

import org.xutils.image.ImageOptions;
import org.xutils.x;

public class AccountFragment extends Fragment implements SuperTextView.OnSuperTextViewClickListener {

    private SuperTextView mLogout;
    private SuperTextView mUserInfo;
    private SuperTextView mPasswd;
    private SuperTextView mRead;
    private SuperTextView mRanking;
    private SuperTextView mExchange;
    private RadiusImageView mAccountImg;

    private ImageOptions imageOptions;

    private SQLiteDatabase db ;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_account,container,false);

        initViews(v);
        initListener();
        db = DBHelper.getDB(this.getContext());

        return v;
    }

    private void initViews(View v) {
        mLogout = v.findViewById(R.id.menu_logout);
        mUserInfo = v.findViewById(R.id.menu_user_info);
        mPasswd = v.findViewById(R.id.menu_user_passwd);
        mAccountImg = v.findViewById(R.id.riv_head_pic);
        mRead = v.findViewById(R.id.tv_read_his);
        mRanking = v.findViewById(R.id.tv_ranking);
        mExchange = v.findViewById(R.id.tv_exchange);

        mExchange.setRightString("剩余 "+GlobalDataHolder.getInstance().getCurUser().getScore()+" 积分");
    }

    private void initListener() {
        mLogout.setOnSuperTextViewClickListener(this);
        mUserInfo.setOnSuperTextViewClickListener(this);
        mPasswd.setOnSuperTextViewClickListener(this);
        mRead.setOnSuperTextViewClickListener(this);
        mRanking.setOnSuperTextViewClickListener(this);
        mExchange.setOnSuperTextViewClickListener(this);
        imageOptions = new ImageOptions.Builder()
                // 如果ImageView的大小不是定义为wrap_content, 不要crop.
                .setCrop(true) // 很多时候设置了合适的scaleType也不需要它.
                // 加载中或错误图片的ScaleType
                //.setPlaceholderScaleType(ImageView.ScaleType.MATRIX)
                .setImageScaleType(ImageView.ScaleType.CENTER_CROP)
                .setPlaceholderScaleType(ImageView.ScaleType.CENTER_CROP)
                .setLoadingDrawableId(R.drawable.ic_default_head)
                .setFailureDrawableId(R.drawable.ic_default_head)
                .setIgnoreGif(false)
                .build();
        x.image().bind(mAccountImg, GlobalDataHolder.getInstance().getCurUser().getAvatar(HTTPConfig.API+HTTPConfig.IMG_PREFIX),imageOptions);
    }

    @Override
    public void onResume() {
        super.onResume();
        x.image().bind(mAccountImg, GlobalDataHolder.getInstance().getCurUser().getAvatar(HTTPConfig.API+HTTPConfig.IMG_PREFIX),imageOptions);
    }

    @Override
    public void onClick(SuperTextView superTextView) {
        int id = superTextView.getId();
        if (id == R.id.menu_logout){
            DBHelper.cleanDB(db);
            Ajax.cleanToken();
            Intent it = new Intent(getContext(), BeforeLoginActivity.class);
            startActivity(it);
            this.getActivity().finish();
        }
        if (id == R.id.menu_user_info){
            Intent it = new Intent(getContext(), AccountSettingActivity.class);
            startActivity(it);
        }
        if (id == R.id.menu_user_passwd){
            Intent it = new Intent(getContext(), PasswordActivity.class);
            startActivity(it);
        }
        if (id == R.id.tv_read_his){
            Intent it = new Intent(getContext(), LearningHistoryActivity.class);
            startActivity(it);
        }
        if (id == R.id.tv_exchange){
            Intent it = new Intent(getContext(), ExchangeHistoryActivity.class);
            startActivity(it);
        }
        if (id == R.id.tv_ranking){
            Intent it = new Intent(getContext(), RankingActivity.class);
            startActivity(it);
        }
    }
}