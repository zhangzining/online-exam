package com.zll.onlineExam.fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alibaba.fastjson.JSONObject;
import com.scwang.smart.refresh.footer.ClassicsFooter;
import com.scwang.smart.refresh.header.ClassicsHeader;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.listener.OnLoadMoreListener;
import com.scwang.smart.refresh.layout.listener.OnRefreshListener;
import com.zll.onlineExam.R;
import com.zll.onlineExam.activities.ProductionContentActivity;
import com.zll.onlineExam.adapter.ProductionItemViewAdapter;
import com.zll.onlineExam.ajax.Ajax;
import com.zll.onlineExam.ajax.HTTPConfig;
import com.zll.onlineExam.ajax.ReqCallback;
import com.zll.onlineExam.entity.out.ProductionDtoOut;
import com.zll.onlineExam.utils.XToastUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StoreFragment extends Fragment {
    private RefreshLayout mRefreshLayout;
    private RecyclerView mRecyclerView;
    private ProductionItemViewAdapter mAdapter;
    private Map<String,Object> parmas;
    private int curPage = 1;
    private int curRows = 5;

    private static Handler mHandler;
    private static DataHolder mRefresh;
    private static DataHolder mLoadMore;

    private final int LOAD_SUCCESS = 3;
    private final int LOAD_FAIL = 2;
    private final int FETCH_SUCCESS = 1;
    private final int FETCH_FAIL = 0;
    private final int FETCH_ERROR = -1;


    private class DataHolder{
        private List<ProductionDtoOut> data;
        private RefreshLayout refreshLayout;

        public List<ProductionDtoOut> getData() {
            return data;
        }

        public DataHolder setData(List<ProductionDtoOut> data) {
            this.data = data;
            return this;
        }

        public RefreshLayout getRefreshLayout() {
            return refreshLayout;
        }

        public DataHolder setRefreshLayout(RefreshLayout refreshLayout) {
            this.refreshLayout = refreshLayout;
            return this;
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_store,container,false);
        initView(v);
        initListener();
        initHandler();

        mRefresh = new DataHolder();
        mLoadMore = new DataHolder();

        return v;
    }
    private void initView(View v) {
        mRefreshLayout = v.findViewById(R.id.refreshLayout);
        mRecyclerView = v.findViewById(R.id.recyclerView);
        parmas = new HashMap<>();
        parmas.put("page","");
        parmas.put("row","");
    }

    private void initListener() {
        mRefreshLayout.setRefreshHeader(new ClassicsHeader(getContext()));
        mRefreshLayout.setRefreshFooter(new ClassicsFooter(getContext()));
        mRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                mRefresh.setRefreshLayout(refreshlayout);
                refreshData(curPage,curRows);
            }
        });
        mRefreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(RefreshLayout refreshlayout) {
                mLoadMore.setRefreshLayout(refreshlayout);
                curRows+=5;
                loadData(curPage,curRows);
            }
        });

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mAdapter = new ProductionItemViewAdapter(new ArrayList<>(), this::gotoProduction,false);
        mRecyclerView.setAdapter(mAdapter);

        // 自动首次刷新
        mRefreshLayout.autoRefresh();
    }

    private void gotoProduction(String pid){
        Intent it = new Intent(getContext(), ProductionContentActivity.class);
        it.putExtra("pid",pid);
        startActivity(it);
    }

    private void initHandler() {
        mHandler = new Handler() {
            @Override
            public void handleMessage(@NonNull Message msg) {
                super.handleMessage(msg);
                switch (msg.what) {
                    case FETCH_SUCCESS:
                        mAdapter.setData(mRefresh.getData());
                        mRefresh.getRefreshLayout().finishRefresh(true);
                        break;
                    case LOAD_SUCCESS:
                        if(mAdapter.getDataList().size() == mLoadMore.getData().size()){
                            XToastUtils.info("没有更多数据了");
                        }
                        mLoadMore.getRefreshLayout().finishLoadMore(true);
                        mAdapter.setData(mLoadMore.getData());
                        break;
                    case FETCH_FAIL:
                        mRefresh.getRefreshLayout().finishRefresh(false);
                        break;
                    case LOAD_FAIL:
                        mLoadMore.getRefreshLayout().finishLoadMore(false);
                        break;
                    case FETCH_ERROR:
                        XToastUtils.error("网络异常");
                        break;
                    default:
                        break;

                }

            }
        };
    }


    public void refreshData(int page,int row){
        parmas.put("page", String.valueOf(page));
        parmas.put("row", String.valueOf(row));
        Ajax.post(HTTPConfig.LIST_PRODUCTION, parmas, new ReqCallback() {
            @Override
            public void onSuccess(JSONObject resp) {
                List<ProductionDtoOut> dtoOutList = resp.getJSONArray("data").toJavaList(ProductionDtoOut.class);
                mRefresh.setData(dtoOutList);
                mHandler.sendEmptyMessage(FETCH_SUCCESS);
            }

            @Override
            public void onFail(String msg) {
                mHandler.sendEmptyMessage(FETCH_FAIL);
            }

            @Override
            public void onError(String msg) {
                mHandler.sendEmptyMessage(FETCH_ERROR);
            }
        });
    }

    public void loadData(int page,int row){
        parmas.put("page", String.valueOf(page));
        parmas.put("row", String.valueOf(row));
        Ajax.post(HTTPConfig.LIST_PRODUCTION, parmas, new ReqCallback() {
            @Override
            public void onSuccess(JSONObject resp) {
                List<ProductionDtoOut> dtoOutList = resp.getJSONArray("data").toJavaList(ProductionDtoOut.class);
                mLoadMore.setData(dtoOutList);
                mHandler.sendEmptyMessage(LOAD_SUCCESS);
            }

            @Override
            public void onFail(String msg) {
                mHandler.sendEmptyMessage(LOAD_FAIL);
            }

            @Override
            public void onError(String msg) {
                mHandler.sendEmptyMessage(FETCH_ERROR);
            }
        });
    }

}