package com.zll.onlineExam.utils;


import android.util.Base64;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.UUID;

public class Base64Util {
    public static String decode(String s){
        return new String(Base64.decode(s, Base64.NO_WRAP));
    }
    public static String fullDecode(String s) throws UnsupportedEncodingException {
        return URLDecoder.decode(decode(s),"UTF-8");
    }
    public static String encode(String s){
        return Base64.encodeToString(s.getBytes(StandardCharsets.UTF_8),Base64.NO_WRAP);
    }
    public static String newUUID(){
        return UUID.randomUUID().toString().replace("-","");
    }
}
