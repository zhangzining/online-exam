package com.zll.onlineExam.utils;

import com.zll.onlineExam.entity.out.UserDtoOut;

/**
 * @name DataHolder
 * @date 2022/3/7 16:36
 */
public class GlobalDataHolder {
    private UserDtoOut curUser;
    private static GlobalDataHolder instance;

    private GlobalDataHolder(){

    }

    public static GlobalDataHolder getInstance(){
        if (instance == null){
            instance = new GlobalDataHolder();
        }
        return instance;
    }

    public UserDtoOut getCurUser() {
        return curUser;
    }

    public GlobalDataHolder setCurUser(UserDtoOut curUser) {
        this.curUser = curUser;
        return this;
    }
}
