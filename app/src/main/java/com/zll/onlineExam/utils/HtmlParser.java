package com.zll.onlineExam.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.Gravity;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.xuexiang.xui.utils.DensityUtils;
import com.xuexiang.xutil.common.StringUtils;
import com.zll.onlineExam.R;
import com.zll.onlineExam.ajax.HTTPConfig;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.xutils.image.ImageOptions;
import org.xutils.x;


/**
 * @name HtmlParser
 * @date 2022/3/23 15:44
 */
public class HtmlParser {
    private final Context context;
    private final String html;
    private final ImageOptions imageOptions;

    final static String P = "p";
    final static String A = "a";
    final static String H1 = "h1";
    final static String H2 = "h2";
    final static String STRONG = "strong";
    final static String EM = "em";
    final static String IMG = "img";
    final static String INDENT = "\t\t";

    private final static String TAG = "===HtmlParse";

    public HtmlParser(String html, Context context) {
        this.html = html;
        this.context = context;
        this.imageOptions = new ImageOptions.Builder()
                // 如果ImageView的大小不是定义为wrap_content, 不要crop.
//                .setCrop(true) // 很多时候设置了合适的scaleType也不需要它.
                // 加载中或错误图片的ScaleType
                //.setPlaceholderScaleType(ImageView.ScaleType.MATRIX)
                .setImageScaleType(ImageView.ScaleType.CENTER_CROP)
                .setPlaceholderScaleType(ImageView.ScaleType.CENTER_CROP)
                .setLoadingDrawableId(R.drawable.ic_default_head)
                .setFailureDrawableId(R.drawable.ic_default_head)
                .setIgnoreGif(false)
                .build();
    }

    public void parseAndGen(LinearLayout layout) {
        Document document = Jsoup.parse(this.html);
        Elements children = document.body().children();

        for (Element ele : children) {
            String tagName = ele.tagName();
            TextView tv = null;
            if (StringUtils.equals(tagName, P)) {
                tv = new TextView(this.context);
                tv.setText(INDENT+ele.text());
                intoTextChildren(tv, ele,layout);
            }
            if (StringUtils.equals(tagName, H1)) {
                tv = new TextView(this.context);
                tv.setTextSize(DensityUtils.sp2px(this.context, 15));
                tv.setText(INDENT+ele.text());
                intoTextChildren(tv, ele,layout);
            }
            if (StringUtils.equals(tagName, H2)) {
                tv = new TextView(this.context);
                tv.setTextSize(DensityUtils.sp2px(this.context, 10));
                tv.setText(INDENT+ele.text());
                intoTextChildren(tv, ele,layout);
            }

            if (tv != null && StringUtil.isNotBlank(tv.getText())){
                Log.d(TAG, "parse TEXT"+tv.getText());
                tv.setPadding(DensityUtils.dp2px(this.context, 2),DensityUtils.dp2px(this.context, 2),DensityUtils.dp2px(this.context, 2),DensityUtils.dp2px(this.context, 2));
                tv.setTextColor(this.context.getResources().getColor(R.color.black));
                layout.addView(tv, LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            }
        }

    }

    private void intoTextChildren(TextView view, Element element, LinearLayout layout) {
        String tagName = element.tagName();
        if (StringUtils.equals(tagName, STRONG)) {
            if (StringUtils.equals(element.parent().tagName(), EM)) {
                view.setTypeface(Typeface.SANS_SERIF, Typeface.BOLD_ITALIC);
            } else {
                view.setTypeface(Typeface.SANS_SERIF, Typeface.BOLD);
            }
        }
        if (StringUtils.equals(tagName, EM)) {
            if (StringUtils.equals(element.parent().tagName(), STRONG)) {
                view.setTypeface(Typeface.SANS_SERIF, Typeface.BOLD_ITALIC);
            } else {
                view.setTypeface(Typeface.SANS_SERIF, Typeface.ITALIC);
            }
        }
        if (StringUtils.equals(tagName, A)) {
            TextView link = new TextView(context);
            link.setPadding(DensityUtils.dp2px(this.context, 2),DensityUtils.dp2px(this.context, 2),DensityUtils.dp2px(this.context, 2),DensityUtils.dp2px(this.context, 2));
            link.setAutoLinkMask(0x01);
            String href = element.attr("href");
            if (!href.startsWith("http")) {
                href = HTTPConfig.HOST + href;
            }
            link.setText(href);
            layout.addView(link,LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT);
            return;
        }
        if (StringUtils.equals(tagName, IMG)) {
            ImageView iv = new ImageView(this.context);
            iv.setPadding(DensityUtils.dp2px(this.context, 2),DensityUtils.dp2px(this.context, 2),DensityUtils.dp2px(this.context, 2),DensityUtils.dp2px(this.context, 2));
            String src = element.attr("src");
            if (!src.startsWith("http")) {
                src = HTTPConfig.API+ HTTPConfig.IMG_PREFIX + src;
            }
            Log.d(TAG, "parse IMG"+src);
            x.image().bind(iv,src,this.imageOptions);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT);
            params.gravity = Gravity.CENTER_HORIZONTAL;
            layout.addView(iv,params);
            return;
        }
        if (element.childrenSize()>0){
            for (Element ele : element.children()) {
                intoTextChildren(view,ele,layout);
            }
        }
    }


}
