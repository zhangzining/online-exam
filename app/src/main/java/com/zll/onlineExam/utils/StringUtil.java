package com.zll.onlineExam.utils;

/**
 * @name StringUtil
 * @date 2022/3/5 17:04
 */
public class StringUtil {
    public static boolean isBlank(String str){
        return str == null || "".equals(str);
    }
    public static boolean isNotBlank(CharSequence str){
        return str != null && !"".equals(str);
    }

    public static boolean equals(CharSequence cs1, CharSequence cs2) {
        if (cs1 == cs2) {
            return true;
        } else if (cs1 != null && cs2 != null) {
            if (cs1.length() != cs2.length()) {
                return false;
            } else if (cs1 instanceof String && cs2 instanceof String) {
                return cs1.equals(cs2);
            } else {
                int length = cs1.length();

                for(int i = 0; i < length; ++i) {
                    if (cs1.charAt(i) != cs2.charAt(i)) {
                        return false;
                    }
                }

                return true;
            }
        } else {
            return false;
        }
    }

    public static String from(Object obj) {
        if (obj == null){
            return "";
        }
        try{
            return String.valueOf(obj);
        }catch (Exception e){
            return "";
        }
    }
    public static String from(Object obj,String defaultStr) {
        if (obj == null){
            return defaultStr;
        }
        try{
            return String.valueOf(obj);
        }catch (Exception e){
            return defaultStr;
        }
    }
}
